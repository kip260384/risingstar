<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Repositories\LogRepository;

class AdminController extends BaseController
{
    // use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(LogRepository $log)
    {
        $this->log = $log;
    }
    
    public function visits()
    {
        $data['visits'] = $this->log->getVisits();

        // dd($data['visits']['2017-06-19']);

        return view('admin.visits', $data);
    }

}
