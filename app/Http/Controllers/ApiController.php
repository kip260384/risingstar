<?php

namespace App\Http\Controllers;

// use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
// use Illuminate\Foundation\Validation\ValidatesRequests;
// use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

use App\Repositories\CatalogRepository;
use App\Repositories\EngineerRepository;
use App\Repositories\UserRepository;
use App\Repositories\SkillRepository;
use App\Repositories\StarRepository;
use App\Repositories\logRepository;

use LdapAuth;

class ApiController extends BaseController
{
    // use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function engineers(EngineerRepository $engineer, Request $request)
    {
        $filter = $request->all();

        // dd($filter);

        $engineers = $engineer->getByFilter($filter);

        return response()->json(['data' => $engineers]);
    }

    public function getusersbyfio(Request $request)
    {
        $query = $request->input('query.term');
        $users = $this->user->getusers($query);

        return response()->json(['data' => $users]);
    }

    public function generateSkillsForBubbleChart(SkillRepository $skill)
    {
        // $start = new \DateTime();
        // echo $start->format('H:i:sP') . '<br>';
        $skills = $skill->getSkillsByHierarchy();
        // echo json_encode($skills) . '<br>';
        // $stop = new \DateTime();
        // echo $stop->format('H:i:sP');
        return response()->json([
            'name' => 'flare',
            'children' => $skills
        ]);
    }

    public function get_skills_by_cat(Request $request, SkillRepository $skill_rep){
        $cat_arr = $request->input('skills_arr');
        
        // if(!$cat_arr or count($cat_arr) === 0){
        //     print '0';
        //     return;
        // }

        session(['skillsgr_filter' => $cat_arr]);

        $skills = $skill_rep->get_active_skills_by_cats($cat_arr);
        $result = $skill_rep->collect_skill_data($skills);

        return response()->json(['data' => $result]);
    }

    public function dataVisitChart(Request $request, LogRepository $log)
    {
        $visits = $log->getVisits();
        // dd($visits);

        foreach ($visits as $day => $visit) {
            $views = 0;
            foreach ($visit as $person) {
                $views += $person->count();
            }
            $data[] = [
                'day' => $day,
                'users' => $visit->count(),
                'views' => $views
            ];
        }

        // dd($data);

        // $starsByMonth = [];

        // foreach ($stars as $star) {
        //     $month = date('Y-m', strtotime($star->SkillLevels_DT));
        //     if ($star->SkillLevels_DT !== null) {
        //         $starsByMonth[$month] = isset($starsByMonth[$month]) ? $starsByMonth[$month] + $star->SkillLevel : $star->SkillLevel;
        //     }
        // }

        // foreach ($starsByMonth as $key => $value) {
        //     $data[] = [
        //         'month' => $key,
        //         'stars' => $value
        //     ];
        // }

        return response()->json($data);
    }

    public function dataRadarChart($EmployeeID = '', Request $request, StarRepository $star)
    {
        // dd($request);

        $user = LdapAuth::user();

        if ($EmployeeID !== '') {
            $user_id = $EmployeeID;
            $position = $request->input('position') ? $request->input('position') : $this->user->getPosition($user_id);
        } else {
            $user_id = $user->employeeid;
            $position = $request->input('position') ? $request->input('position') : $user->title;
        }

        $skillsbyposition = $star->getStarsByPosition($position);
        $skillsbyposition =collect(json_decode(json_encode($skillsbyposition), true));
        $keyed1 = $skillsbyposition->mapWithKeys(function ($item) {
            return [
                $item['Name'] => ['NeedLevel' => $item['NeedLevel'], 'ID' => $item['ID']]
            ];
        });
        $keyed['pos'] = $keyed1->all();

        $myskills = $star->getStarsById($user_id);
        $myskills = collect(json_decode(json_encode($myskills), true));
        $keyed2 = $myskills->mapWithKeys(function ($item) {
            return [
                $item['Name'] => ['SkillLevel' => $item['SkillLevel'], 'ID' => $item['ID']]
            ];
        });
        $keyed['my'] = $keyed2->all();

        // $data['data'] = array_merge($keyed['pos'], $keyed['my']);
        $data['data'] = array_replace_recursive($keyed['pos'], $keyed['my']);

        // dd($keyed);

        $data['skillsbyposition'] = $data['myskills'] = $data['labels'] = [];

        foreach ($data['data'] as $key => $row) {
            $data['labels'][] = $key;
            $data['skillsbyposition'][] = isset($row['NeedLevel']) ? intval($row['NeedLevel']) : 0;
            $data['myskills'][] = isset($row['SkillLevel']) ? intval($row['SkillLevel']) : 0;
        }

        // dd($data);

        return response()->json($data);
    }

    function add_wishedcity(EngineerRepository $engineer, Request $request){
        $Employee_ID = LdapAuth::user()->employeeid;
        $city = $request->input('city');
        $engineer->add_wishedcity($Employee_ID, $city);
        print "null";
    }

    function del_wishedcity(EngineerRepository $engineer, Request $request){
        $Employee_ID = LdapAuth::user()->employeeid;
        $city = $request->input('city');
        $engineer->del_wishedcity($Employee_ID, $city);
        print "null";
    }

    function set_WishedPosition(EngineerRepository $engineer, Request $request){
        $Employee_ID = LdapAuth::user()->employeeid;
        $position = $request->input('position');
        $engineer->set_WishedPosition($Employee_ID, $position);
        print "null";
    }
}
