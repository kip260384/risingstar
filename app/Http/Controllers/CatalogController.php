<?php

namespace App\Http\Controllers;

use App\Repositories\CatalogRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Libraries\Catalog_lib;

use Illuminate\Http\Request;
use DB;
use PhpParser\Node\Stmt\Foreach_;

class CatalogController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Catalog_lib $catalog_lib, CatalogRepository $repository)
    {
        $this->catalog_lib = $catalog_lib;
        $this->repository = $repository;
    }

    public function skillcatalog()
    {
		$data['catalog_flat'] = $this->catalog_lib->getcatalog_arr();
		$data['catalogStr'] = $this->catalog_lib->getCatalogStr();
        return view('skillcatalog', $data); 
    }

    public function add_category(Request $request){
        $r = $this->repository->insertCat(
            $request->input('name'),
            $request->input('parent')
        );
		if($r){
            return redirect('skillcatalog')->with('status', 'Категория добавлена');
        }else{
            return redirect('skillcatalog')->with('error', 'Ошибка');
        }

    }

    public function rename_category(Request $request){
        $this->repository->renameCat(
            $request->input('id'),
            $request->input('newname')
        );
        return redirect('skillcatalog')->with('status', 'Категория переименована');
    }

	function del_category(Request $request){
		$id_arr = $request->input('catalog_tree', false);
        if(!$id_arr){
            return redirect('skillcatalog')->with('error', 'Категория не выбрана');
        }

        $this->repository->delCat($id_arr);

		return redirect('skillcatalog');//->with('status', 'Some message');
	}

}
