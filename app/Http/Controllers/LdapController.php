<?php

namespace App\Http\Controllers;

// use Illuminate\Routing\Controller as BaseController;

class LdapController// extends BaseController
{
    private $id_user = 0;
    private $login = '';
    private $uname = '';
    private $email = '';
    private $ldap_user = 'dash_region';
    private $ldap_pwd = 'ObX9FJ7pU0';

    private $dn = array(
        'regions' => array('dn' => 'DC=regions,DC=alfaintra,DC=net', 'domain' => 'regions.alfaintra.net'),
        'moscow' => array('dn' => 'DC=moscow,DC=alfaintra,DC=net', 'domain' => 'moscowdc1.moscow.alfaintra.net'),
        'e-business' => array('dn' => 'DC=e-business,DC=moscow,DC=alfaintra,DC=net', 'domain' => 'director.e-business.moscow.alfaintra.net')
    );

    private $testUsers = [
        'regions\_32104' => [
            "domain" => "regions",
            "login" => "u_32104",
            "uname" => "Киприянов Павел Игоревич",
            "employeeid" => "KEMER186",
            "city" => "Кемерово",
            "email" => "PKipriyanov@alfabank.ru",
            "title" => "Главный инженер",
            "description" => "Киприянов П.И.",
            "physicaldeliveryofficename" => "ОПЕРАЦИОННЫЙ ОФИС КЕМЕРОВСКИЙ",
            "telephonenumber" => "0589223",
            "department" => "Региональный Центр сопровождения ИТ-инфраструктуры Сибирского ре",
        ],
        'regions\u_32107' => [
            "domain" => "regions",
            "login" => "u_32107",
            "uname" => "Ланин Максим Юрьевич",
            "employeeid" => "KEMER398",
            "city" => "Кемерово",
            "email" => "MLanin@alfabank.ru",
            "title" => "Ведущий инженер",
            "description" => "Ланин Максим Юрьевич",
            "physicaldeliveryofficename" => "ОПЕРАЦИОННЫЙ ОФИС КЕМЕРОВСКИЙ",
            "telephonenumber" => "0589225",
            "department" => "Региональный Центр сопровождения ИТ-инфраструктуры Сибирского ре"
        ]
    ];

    function __construct(){
//         $this->login = 'moscow\u_00b27';  // Неволин Игорь Борисович
        $this->login = isset($_SERVER['LOGON_USER']) ? $_SERVER['LOGON_USER'] : 'regions\u_32107';
        $user_info = $this->getDataAD($this->login);
        $this->uname = $user_info["uname"];
        $this->email = $user_info["email"];
    }

    function getUserInfo()
    {
        $user = $this->login;
        if (env('APP_AD') == false && array_key_exists(strtolower($user), $this->testUsers)) {
            return $this->testUsers[strtolower($user)];
        }
        
        return $this->getDataAD($user);
    }

    function getLogin() {
        return $this->login;
    }

    function getFIO() {
        return $this->uname;
    }

    function getEmail() {
        return $this->email;
    }

//    function getDataAD(){
//        return array(
//            'domain'=>'regions',
//            'login'=>'admin',
//            'uname'=>"regions|U_32104",
//            'employeeid'=>"KEMER186",
//            'city'=>"Kemerovo",
//            'email'=>"asd@asd.df",
//            'title'=>"",
//            'description'=>"",
//            'physicaldeliveryofficename'=>"",
//            'telephonenumber'=>"0589223",
//            'department'=>"IT",
//            'thumbnailphoto'=>"",
//        );
//    }

    function getDataAD($user)
    {
        if (env('APP_AD') == false && array_key_exists(strtolower($user), $this->testUsers)) {
            return $this->testUsers[strtolower($user)];
        }

        $login = explode('\\',strtolower(trim($user)));
        $domain = $login[0];
        $uname = $login[1];
        $ldap = ldap_connect($this->dn[$domain]['domain'], 389);

        if (!$ldap) {
            echo "ldap server connection failed...";
            exit;
        }

        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
        $bind = ldap_bind($ldap, $this->ldap_user, $this->ldap_pwd);
        if (!$bind) {
            echo "Error bind";
            exit;
        } else {
            $filter = "(&(objectClass=user)(|(cn=$uname)(sAMAccountName=$uname*)))";
           // $justthese = array("*");
            $justthese = array("displayname", "l", "mail", "title", "description", "employeeid", "physicaldeliveryofficename", "telephonenumber", "department", "lastlogon", "thumbnailphoto");    //, "memberof"
            $sr=ldap_search($ldap, $this->dn[$domain]['dn'], $filter, $justthese);   //
            $res = ldap_get_entries($ldap, $sr);
        }
        ldap_close($ldap);

       // dd($res);

        return array(
            'domain'=>$domain,
            'login'=>$uname,
            'uname'=>isset($res[0]['displayname'][0]) ? $res[0]['displayname'][0] : "",
            'employeeid'=>isset($res[0]['employeeid'][0]) ? $res[0]['employeeid'][0] : "",
            'city'=>isset($res[0]['l'][0]) ? $res[0]['l'][0] : "",
            'email'=>isset($res[0]['mail'][0]) ? $res[0]['mail'][0] : "",
            'title'=>isset($res[0]['title'][0]) ? $res[0]['title'][0] : "",
            'description'=>isset($res[0]['description'][0]) ? $res[0]['description'][0] : "",
            'physicaldeliveryofficename'=>isset($res[0]['physicaldeliveryofficename'][0]) ? $res[0]['physicaldeliveryofficename'][0] : "",
            'telephonenumber'=>isset($res[0]['telephonenumber'][0]) ? $res[0]['telephonenumber'][0] : "",
            'department'=>isset($res[0]['department'][0]) ? $res[0]['department'][0] : "",
            'thumbnailphoto'=>isset($res[0]['thumbnailphoto'][0]) ? $res[0]['thumbnailphoto'][0] : "",
        );
    }

    function getUserADGroup($user)
    {
        $login = explode('\\',strtolower(trim($user)));
        $domain = $login[0];
        $uname = $login[1];

        $ldap = ldap_connect($this->dn[$domain]['domain']);
        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
        $bind = ldap_bind($ldap, $this->ldap_user, $this->ldap_pwd);
        if (!$bind) {
            echo "Error bind";
            ldap_close($ldap);
            return array();
        } else {
            if ($this->connid) {
                $query = 'SELECT * FROM spr_block';
                $res = sqlsrv_query($this->connid, $query);
                while( $row = sqlsrv_fetch_array($res, SQLSRV_FETCH_ASSOC) ) {
                    $filter = "(&(memberOf:1.2.840.113556.1.4.1941:=CN=".$row['ad_group'].",OU=Groups,OU=ROC2,OU=Altay,DC=regions,DC=alfaintra,DC=net)(cn=".$uname."))";
                    $justthese = array("dn");
                    $sr=ldap_search($ldap, $this->dn[$domain]['dn'], $filter, $justthese);
                    $ad_res = ldap_get_entries($ldap, $sr);
                    if ($ad_res['count'] > 0) {
                        $this->block[$row['id']] = $row['value'];
                    }
                }
                ldap_close($ldap);
                return $this->block;
            }
        }
    }

    function getSubstFIO($fio, $city = '')
    {
        $a = $this->getADUserList('regions', $fio, $city);
        $a = array_merge($a, $this->getADUserList('moscow', $fio, $city));
        $a = array_merge($a, $this->getADUserList('e-business', $fio, $city));
        return $a;
    }

    function getADUserList($domen, $fio, $city, $limit = 20)
    {
        $a = array();
        $ldap = ldap_connect($this->dn[$domen]['domain']);
        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
        $bind = ldap_bind($ldap, $this->ldap_user, $this->ldap_pwd);
        if (!$bind) {
            echo "Error bind";
        } else {
            $filter = "(&(objectClass=user)(|(cn=$fio)(displayname=$fio*)))";
//            $filter = "(&(objectClass=user)(|(ou=Krasnoyarsk)(cn=$fio)(displayname=*$fio*)))";
//            $filter = "(&(objectClass=user)(|(cn=$fio)(displayname=*$fio*)))";
//            $filter = "(&(objectClass=user)(l=$city)(!(UserAccountControl:1.2.840.113556.1.4.803:=2))(|(cn=$fio)(displayname=*$fio*)))";
//            $justthese = array("displayname", "cn");//, "physicaldeliveryofficename", "title"
            $justthese = array("displayname", "cn", "mail", "title", "employeeid", "description", "physicaldeliveryofficename", "telephonenumber", "department", "thumbnailphoto");    //, "memberof"
            $sr=ldap_search($ldap, $this->dn[$domen]['dn'], $filter, $justthese );   //,0,$limit  , $justthese
            $res = ldap_get_entries($ldap, $sr);

//            if ($res['count'] == 0) {
//                $filter = "(&(objectClass=user)(|(cn=$fio)(displayname=$fio*)))";
//                $sr = ldap_search($ldap, $this->dn[$domen]['dn'], $filter, $justthese );   //,0,$limit  , $justthese
//                $res = ldap_get_entries($ldap, $sr);
//            }

            for ($i=0; $i < $res["count"]; $i++) {
                $login_name = isset($res[$i]['cn'][0]) ? $res[$i]['cn'][0] : "";
                if ( $login_name != "" ) {
                    $a[$i]['user_ad'] = $domen . '\\' . $login_name;
                    $a[$i]['displayname'] = $res[$i]['displayname'][0];
                    $a[$i]['employeeid'] = isset($res[$i]['employeeid'][0]) ? $res[$i]['employeeid'][0] : "";
                    $a[$i]['title'] = isset($res[$i]['title'][0]) ? $res[$i]['title'][0] : '-';
                    $a[$i]['physicaldeliveryofficename'] = isset($res[$i]['physicaldeliveryofficename'][0]) ? $res[$i]['physicaldeliveryofficename'][0] : '-';
                    $a[$i]['telephonenumber'] = isset($res[$i]['telephonenumber'][0]) ? $res[$i]['telephonenumber'][0] : '-';
                    $a[$i]['department'] = isset($res[$i]['department'][0]) ? $res[$i]['department'][0] : '-';
                    $a[$i]['thumbnailphoto'] = isset($res[$i]['thumbnailphoto'][0]) ? base64_encode($res[$i]['thumbnailphoto'][0]) : 'R0lGODlhYABgAOYAALe3t/39/fv7+9jY2LW1tb+/v/f39+Dg4O/v77q6uu7u7vn5+dTU1N/f38LCwvHx8cTExPz8/Lm5uf7+/vT09LS0tObm5uLi4rOzs8HBwdbW1s3Nze3t7fr6+s/Pz97e3s7OzvX19dDQ0MvLy/j4+N3d3cfHx7y8vNPT09HR0ePj4/b29rKysra2tri4uOrq6tra2sbGxsrKyuzs7OTk5MDAwOHh4eXl5bu7u729venp6dfX176+vuvr69XV1fLy8sXFxfPz89nZ2efn58zMzPDw8Ojo6Nvb28jIyNLS0v///7GxsQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjEgNjQuMTQwOTQ5LCAyMDEwLzEyLzA3LTEwOjU3OjAxICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1LjEgV2luZG93cyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFQUMxRkFFMDgyNTMxMUUxODgzNEFDRkJERUI0MTc3NiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFQUMxRkFFMTgyNTMxMUUxODgzNEFDRkJERUI0MTc3NiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkVBQzFGQURFODI1MzExRTE4ODM0QUNGQkRFQjQxNzc2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkVBQzFGQURGODI1MzExRTE4ODM0QUNGQkRFQjQxNzc2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEAAAAAAAsAAAAAGAAYAAAB/+AS4KDhIWGh4iJiouMjY6PkJGSk5SVlpeYmZqbnJ2en6ChoqOkpaanqKmqq6ytrq+wlAUZtLGnOEIKSru8ERYjBLafDjS8xscBDQDCmgQ2x9DHHQwYzJY1QdHaxjPL1pIQEdvjuxQn35AOAuTsCwnojTkL7PQPFfCKLLr09Bb4iQz4Cdzwz1CCdQLpGQhWcFCDhAIHNBQEIABEfgJaTERxUSCKiRw68kPQEMAEkfze/UOCkh/BfwNa0vtQUIVMdjMK9rhJzkBBBDzHBSj4IOi4e/j2GY2GFN6LpdoKXoAKbUHBHVSPKShoIquxCwVNet2VpGHIsQUaBvQaYmJFrxL/J06lGkDCxCUSEC61cVcQ1qUd7PYlsGKph76DCojj2QMxoRE8KbhwTIhjSwMqKQ8asfhiiHOaCzkoDJGGxtCGKgzoTI7ES9SIEhwxMO4BiGqaIYhocMBHjkQOYBzggKD4BQ1pFRE5YOGDCAfCcAwgAa0BQ0wAnh57wMAbKyDatVHIgMkBbW0BDmQ+BcAmuwA+KmEYYJFcesGlPHRIqGNypAJFCSSACaSwcEBHCxzmiGr1JTRBCqM8hNIPI+AWmwbniSRDKCDwJIAFKUCQAQEFxCDCASF4mFwnAMwz1lIUXLeJhC8uFdcmJ5xU41IZcQLDjlRBqAkL1AG5FAebdGXks1ITeHdJTEsu9dolOkS5FE2ZZGjlTSRhQsCWRgmQSQ1gGoVfJTKUGRQEmKylpkwiYFLCmzfBgIkFdMoE1iVA5YmSEZi46GdHW1ny5aAiUXAJD4iKJKYlMTQqkoWTdCjpRf5RosGlF/FgyZycJsRmJTeEmtCGlShlKjsKUkLaquwwYEmDsI5zoyQA1DpTJQXoyo4KlUTq6zhDVGLpsNrkpCmy2xRRCajMQtMWJaVGC00HiwQCADs=';
                }
            }
        }
        ldap_close($ldap);
        return $a;
    }

    public  function test()
    {
        // $a = $this->getSubstFIO('мачина', 'Красноярск');
       $a = $this->getDataAD('regions\u_32104');
        dd($a);
        return 'test';
    }
}
