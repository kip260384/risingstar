<?php

namespace App\Http\Controllers;

use App\LdapAuth\LdapAuth;
use App\Repositories\SkillRepository;
use App\Repositories\StarRepository;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\News_feed;

use App\Repositories\EngineerRepository;
use App\Http\Controllers\CatalogController;
use App\Libraries\Dashboard_lib;
use App\Libraries\Catalog_lib;

class MainController extends BaseController
{
    public function index(Dashboard_lib $dashboard_lib)
    {
//        $data['cities'] = $engineer->getAllCities();
        $data['pr_appraised'] = $dashboard_lib->get_pr_appraised();
        $data['ex_number'] = $dashboard_lib->get_experts_number();
        $data['rcs_number'] = $dashboard_lib->get_rcs_number();
        $data['stars_count'] = $dashboard_lib->get_starsCount();

        $data['news'] = News_feed::take(5)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('dashboard.index', $data);
    }

    public function profile(EngineerRepository $engineer, SkillRepository $skill, $user_id = ''){
        $data['user'] = $engineer->getAllUserDataByEID($user_id);
        $data['owner'] = \App\Facades\LdapAuth::user()->employeeid == $user_id;
        $data['cities'] = $engineer->getAllCities();
        if ($user_id !== '') {
            $data['WishedCities'] = $engineer->get_WishedCities($user_id);
        } else {
            $data['WishedCities'] = $engineer->get_WishedCities(\App\Facades\LdapAuth::user()->employeeid);
        }

        $data['skills'] = $skill->get_user_skills($user_id);
        
        $data['employee_id'] = $user_id;
        return view('profile.profile', $data);
    }

    public function circle(EngineerRepository $engineer, $user_id){
        if(\App\Facades\LdapAuth::user()->employeeid != $user_id){
            return redirect('/')->with('error', 'Недостаточно прав');
        }

        $data['owner'] = \App\Facades\LdapAuth::user()->employeeid == $user_id;
        $data['cities'] = $engineer->getAllCities();
        $data['WishedCities'] = $engineer->get_WishedCities(\App\Facades\LdapAuth::user()->employeeid);

        $data['Positions'] = $engineer->getPositions('РЦС');
        $data['WishedPosition'] = $engineer->getWishedPosition($user_id);

        $data['employee_id'] = $user_id;
        return view('profile.circle', $data);
    }

    public function rate(EngineerRepository $engineer, $user_id = ''){
        $data['owner'] = \App\Facades\LdapAuth::user()->employeeid == $user_id;
       return view('profile.rate', $data);
    }

    public function skills(Catalog_lib $catalog)
    {
        $skillsgr_filter = session('skillsgr_filter');

        $data['catalogStr'] = $catalog->getCatalogStr('skill_catalog_tree', $skillsgr_filter);
        return view('skills.index', $data);
    }

    public function engineers(EngineerRepository $engineer, Catalog_lib $catalog)
    {
        $data['engineers'] = $engineer->getAll();
        $data['cities'] = $engineer->getAllCities();
        $skills_filter = session('skills_filter');
        $data['skill_filter'] = $catalog->getSkillsStr('skill_filter', $skills_filter);

        return view('engineers.index', $data);
    }

    public function ldap()
    {
        $data['user_fio'] = $this->ldap->getFIO();
        $data['user_login'] = $this->ldap->getLogin();

        return 'test';
    }

    public function rise($user_id, Catalog_lib $catalog, SkillRepository $skill_rep, EngineerRepository $engineer)
    {
        if(\App\Facades\LdapAuth::hasRole('admin')){
            $data['catalogStr'] = $catalog->getSkillsStr('skills_tree');
        }else{
            $data['catalogStr'] = $catalog->getManagerSkillsStr('skills_tree');
        }

        $data['user_id'] = $user_id;
        $data['FIO'] = $engineer->getFIO($user_id);
        $data['skills'] = $skill_rep->get_user_skills($user_id);

        return view('rise', $data);
    }

    function test(SkillRepository $skill_rep){
        return view('test');
        $cat_arr = array(17, 32);

        $skills = $skill_rep->get_active_skills_by_cats($cat_arr);
        $result = $skill_rep->collect_skill_data($skills);

        print json_encode($result);

//        dd($skills);
    }

    public function moderator(SkillRepository $skill_rep){
        if(\App\Facades\LdapAuth::authorize('open_moderate')){
            $data['title'] = 'Новые компетенции';
            $data['Skills'] = $skill_rep->get_new_skills();
            return view('moderator.index', $data);
        }else{
            return redirect("/");
        }
    }

    public function adm_positions(EngineerRepository $engineer){
        if(!\App\Facades\LdapAuth::hasRole('admin')){
            return redirect('/')->with('error', 'Недостаточно прав');
        }

        $data['Positions'] = $engineer->getPositions('РЦС');
        return view('admin.positions', $data);
    }

    public function adm_positionprofile($pos_id, SkillRepository $skill_rep, Catalog_lib $catalog){
        if(!\App\Facades\LdapAuth::hasRole('admin')){
            return redirect('/')->with('error', 'Недостаточно прав');
        }

        $data['catalogStr'] = $catalog->getSkillsStr('skills_tree');
        $data['position_id'] = $pos_id;
        $data['Position'] = DB::table('Positions')->where('ID', $pos_id)->value('Position');
        $data['skills'] = $skill_rep->get_position_skills($pos_id);

        return view('admin.profiles', $data);
    }

    public function manager_engineers(EngineerRepository $engineer){
        if(!\App\Facades\LdapAuth::hasRole('manager')){
            return redirect('/')->with('error', 'Недостаточно прав');
        }

        $data['engineers'] = $engineer->getAll();
        $data['cities'] = $engineer->getAllCities();

        return view('engineers.index', $data);

    }

}
