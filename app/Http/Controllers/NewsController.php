<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Facades\LdapAuth;
use Illuminate\Http\Request;
use App\Libraries\News_lib;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\News_feed;


class NewsController extends BaseController
{
    use ValidatesRequests;

    public function index(){
        echo("news");
    }

    public function add_news(){
        if(!LdapAuth::hasRole('admin')){
            return redirect('/')->with('error', 'Недостаточно прав');
        }

        $data['types'] = [
            ['Value' => 'NEWS', 'DisplayText' => 'NEWS'],
            ['Value' => 'SKILL', 'DisplayText' => 'SKILL'],
        ];

        return view('admin.add_news', $data);
    }

    public function add_news_post(Request $request, News_lib $news_lib)
    {
        $this->validate($request, [
            'Type' => 'required',
            'Body' => 'required'
        ]);

        $news_lib->add_News([
            'Owner' => 0,
            'Type' => $request->input('Type'),
//            'Header' => $request->input('Header'),
            'Body' => $request->input('Body')
        ]);

        return redirect('/')->with('status', 'Новость добавлена');
    }

    public function del_news($id){
        $authorized = LdapAuth::authorize('del_news');
        if(!$authorized){
            return redirect('/')->with('error', 'Недостаточно прав');
        }


        if(!\App\Facades\LdapAuth::hasRole('admin')){
            return redirect('/')->with('error', 'Недостаточно прав');
        }

        if($id > 0){
            News_feed::where('ID', $id)->delete();
        }

        return redirect('/')->with('status', 'Новость удалена');

    }
}