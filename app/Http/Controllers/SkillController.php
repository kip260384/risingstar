<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\CatalogController;

use Illuminate\Http\Request;
use DB;
use App\Facades\LdapAuth;
use App\Repositories\SkillRepository;
use App\Libraries\Catalog_lib;
use App\Libraries\News_lib;

class SkillController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function __construct(SkillRepository $repository)
    {
        $this->repository = $repository;
    }

    public function skillcatalog(Catalog_lib $catalog_lib)
    {
        $data['catalog_flat'] = $catalog_lib->getcatalog_arr();
        $data['catalogStr'] = $catalog_lib->getCatalogStr();
        return view('skillcatalog', $data);

    }

    public function addskill(Catalog_lib $catalog_lib)
    {
        $data = array(
            'id' => null,
            'catalog_flat' => $catalog_lib->getcatalog_arr(),
            'name' => null,
            'skillgroup' => null,
            'position' => null,
            'materials' => null,
            'desc' => null,
            'manager_id' => null,
            'manager_fio' => null,
            'expert_id' => null,
            'expert_fio' => null,
            'desc1' => null,
            'desc2' => null,
            'desc3' => null,
            'test1' => null,
            'test2' => null,
            'test3' => null,
            'submit_name' => 'Добавить'
        );

        return view('addskill', $data);
    }

    public function delskill($id){
        $authorized = LdapAuth::authorize('delete_skill');

        if($id > 0 && $authorized){
            DB::table('Skills')->where('ID', $id)->delete();
            DB::table('StarMapRights')->where('SMElementID', $id)->delete();
            DB::table('SkillLevels')->where('Skills_ID', $id)->delete();
        }else{
            return redirect('skills')->with('error', 'Недостаточно прав');
        }

        return redirect('skills')->with('status', 'Компетенция удалена');
    }

    public function editskill($id, Catalog_lib $catalog_lib){
        $authorized = LdapAuth::authorize('update_skill', $id);
        if(!$authorized){
            return redirect('skills')->with('error', 'Недостаточно прав');
        }

        $data = array(
            'id' => $id,
            'catalog_flat' => $catalog_lib->getcatalog_arr(),
            'name' => '',
            'skillgroup' => '0',
            'position' => null,
            'materials' => null,
            'desc' => '',
            'manager_id' => '',
            'manager_fio' => '',
            'expert_id' => '',
            'expert_fio' => '',
            'desc1' => '',
            'desc2' => '',
            'desc3' => '',
            'test1' => null,
            'test2' => null,
            'test3' => null,
            'submit_name' => 'Сохранить'
        );

        $skill = DB::table('Skills')->select('ID', 'Name', 'SkillsGroupID', 'Description', 'Position', 'Materials', 'Status')
            ->where('ID', $id)
            ->first();
        $data['skill'] = $skill;
        $data['name'] = $skill->Name;
        $data['skillgroup'] = $skill->SkillsGroupID;
        $data['desc'] = $skill->Description;
        $data['position'] = $skill->Position;
        $data['materials'] = $skill->Materials;

        //Достаю менеджера и эксперта
        $men = $this->repository->get_skill_manex($skill->ID);

        foreach ($men as $man){
            if($man->SMRolesID == 1){
                $data['manager_fio'] .= $man->FIO;
                $data['manager_id'] .= $man->EmployeeID;
            }
            if($man->SMRolesID == 2){
                $data['expert_fio'] .= $man->FIO;
                $data['expert_id'] .= $man->EmployeeID;
            }
        }

        $descs = $this->repository->get_skill_levels_data($skill->ID);

        foreach ($descs as $d){
            $data['desc'.$d->SkillLevel] = $d->Description;
            $data['test'.$d->SkillLevel] = $d->Test;
        }

        if($skill->Status == 5){
            $data['comments'] = $this->repository->get_moderating_history($id);
            $data['submit_name'] = 'На рассмотрение';
        }

        return view('addskill', $data);
    }

    function addskill_post(Request $request){
        $name = $request->input('name');
        $sid = $request->input('skillgroup');
        $desc = $request->input('desc');
        $position = $request->input('position');
        $materials = $request->input('materials');
        $manager_id = $request->input('manager_id');
        $expert_id = $request->input('expert_id');
//        $FIO_m = $request->input('manager_fio');
//        $FIO_e = $request->input('expert_fio');
        $desc1 = $request->input('desc1');
        $desc2 = $request->input('desc2');
        $desc3 = $request->input('desc3');
        $test1 = $request->input('test1');
        $test2 = $request->input('test2');
        $test3 = $request->input('test3');
        $your_comment = $request->input('your_comment');

        $send = isset($_POST['send']);

        $id = $request->input("id");
        if($id > 0){ //Редактирование существующей компетенции
            $this->validate($request, [
                'name' => 'required',
                'skillgroup' => 'required|min:1',
                'manager_id' => 'required',
                'desc' => 'required',
            ]);

            $update = true;

            $authorized = LdapAuth::authorize('update_skill', $id);
            if(!$authorized){
                return redirect('skills')->with('error', 'Недостаточно прав');
            }

            //Лог модерации
            $old_status = $this->repository->get_skill_status($id);
            if($old_status == 5 && $send){
                DB::table('Moderating')->insert([
                    'StatusID' => 1, 'TableName' => 'Skills', 'PK_ID' => $id, 'UserName' => LdapAuth::user()->employeeid, 'Description' => $your_comment, 'Date' => new \DateTime()
                ]);
            }

            if(!$send){
                if($old_status == 2){
                    $new_status = 6; //Активная переходит в черновик
                }else{
                    $new_status = $old_status;  //Остаётся в статусе доработка
                }
            }else{
                $new_status = 1;
            }
            $this->repository->update_skill(
                $id, ['Name' => $name, 'Description' => $desc, 'Position' => $position, 'Materials' => $materials, 'Status' => $new_status]
            );
        }else{//Добавление новой компетенции
            $this->validate($request, [
                'name' => 'required|unique:Skills,Name',
                'skillgroup' => 'required|min:1',
                'manager_id' => 'required',
                'desc' => 'required',
            ]);
            $update = false;
            if($send){
                $new_status = 1;
            }else{
                $new_status = 6; //Черновик
            }
            $id = $this->repository->insert_skill(['Name' => $name, 'SkillsGroupID' => $sid, 'Status' => $new_status, 'Description' => $desc, 'Position' => $position, 'Materials' => $materials]);
        }

        if($id>0){
            if($update){
                $this->repository->update_skill_manex($id, $manager_id, $expert_id);
                $this->repository->update_skill_levels_data($id, compact('test1', 'test2', 'test3', 'desc1', 'desc2', 'desc3'));

                return redirect('skills')->with('status', 'Скилл обновлен');
            }else{
                $this->repository->add_skill_manex($id, $manager_id, $expert_id);
                $this->repository->add_skill_levels_data($id, compact('test1', 'test2', 'test3', 'desc1', 'desc2', 'desc3'));

                return redirect('addskill')->with('status', 'Скилл добавлен');
            }
        }else{
            return redirect('addskill')->with('error', 'Ошибка')->withInput($request->input());
        }

    }

    function viewskill($id, SkillRepository $repository){
        if(!LdapAuth::authorize('view_skill', $id)){
            return redirect('/');
        }

        $data['skill'] = $this->repository->get_skill($id);
        $data['lvls'] = $this->repository->get_skill_levels_data($id);
        $data['men'] = $this->repository->get_skill_manex($id);

        $data['SkillLevel'] = $this->repository->get_skilllevel_for_user($id, \App\Facades\LdapAuth::user()->employeeid);

        $data['admin'] = LdapAuth::hasRole('admin');
        $data['manager'] = false;
        if(session()->has('roles')){
            $roles = session('roles');
            if(array_key_exists('manager', $roles)){
                if(in_array($data['skill']->ID, $roles['manager'])){
                    $data['manager'] = true;
                }
            }
        }

        $data['SkillPath'] = $this->repository->get_full_cat_path($id);
        $data['comments'] = $repository->get_moderating_history($id);

        return view('viewskill', $data);
    }

    function adduserskills(Request $request){
        $skills = $request->input('catalog_tree');
        $user_id = $request->input('user_id');

        foreach ($skills as $skill_id){
            if($skill_id < 0){continue;}
            $null_level_id = $this->repository->get_null_level_for_skill($skill_id);

            if($null_level_id > 0){
                DB::table('StaffProgress')->insert([
                   'Staff_ID' => $user_id, 'SkillLevels_ID' => $null_level_id, 'SkillLevels_DT' => new \DateTime()
                ]);
            }
        }

        return redirect("rise/$user_id");
    }

    function add_position_skills(Request $request){
        if(!LdapAuth::hasRole('admin')){
            return redirect('/')->with('error', 'Недостаточно прав');
        }

        $skills = $request->input('catalog_tree');
        $pos_id = $request->input('position_id');

        foreach ($skills as $skill_id){
            if($skill_id < 0){continue;}
            $null_level_id = $this->repository->get_null_level_for_skill($skill_id);

            if($null_level_id > 0){
                DB::table('SMPositions')->insert([
                    'Position_ID' => $pos_id, 'SkillLevel_ID' => $null_level_id
                ]);
            }
        }

        return redirect("adm_positionprofile/$pos_id");
    }

    public function riseuserskills(Request $request){
        $skills = $request->input('Skills'); // [skill_id] => skill_level
        $user_id = $request->input('user_id');

        foreach ($skills as $skill_id => $skill_lvl){
            //id всех уровней скила
            $lvls = $this->repository->get_skill_levels_ids($skill_id);

            //id целевого уровня и скила
            $target_lvl_id = 0;
            $lvls_arr = array();
            foreach ($lvls as $lvl){
                $lvls_arr[] = $lvl->ID;
                if($lvl->SkillLevel == $skill_lvl){
                    $target_lvl_id = $lvl->ID;
                }
            }
            if(!$target_lvl_id > 0){continue;}

            //Проверить есть уже какой-либо уровень или нет, если есть - update, нет - insert
            $exsisted_level = DB::table('StaffProgress')->select('ID', 'SkillLevels_ID')
                ->whereIn('SkillLevels_ID', $lvls_arr)
                ->where('Staff_ID', $user_id)
                ->first();

            if(count($exsisted_level) > 0){
                if($exsisted_level->SkillLevels_ID != $target_lvl_id){
                    DB::table('StaffProgress')
                        ->where('ID', $exsisted_level->ID)
                        ->update(['SkillLevels_ID' => $target_lvl_id, 'SkillLevels_DT' => new \DateTime()]);
                }
            }else{
                DB::table('StaffProgress')->insert([
                    'Staff_ID' => $user_id, 'SkillLevels_ID' => $target_lvl_id, 'SkillLevels_DT' => new \DateTime()
                ]);
            }
        }

        return redirect("rise/$user_id");
    }

    function save_position_skills(Request $request){
        if(!LdapAuth::hasRole('admin')){
            return redirect('/')->with('error', 'Недостаточно прав');
        }

        $skills = $request->input('Skills'); // [skill_id] => skill_level
        $pos_id = $request->input('position_id');

        foreach ($skills as $skill_id => $skill_lvl){
            //id всех уровней скила
            $lvls = $this->repository->get_skill_levels_ids($skill_id);

            //id целевого уровня и скила
            $target_lvl_id = 0;
            $lvls_arr = array();
            foreach ($lvls as $lvl){
                $lvls_arr[] = $lvl->ID;
                if($lvl->SkillLevel == $skill_lvl){
                    $target_lvl_id = $lvl->ID;
                }
            }
            if(!$target_lvl_id > 0){continue;}

            //Проверить есть уже какой-либо уровень или нет, если есть - update, нет - insert
            $exsisted_level = DB::table('SMPositions')->select('ID', 'SkillLevel_ID')
                ->whereIn('SkillLevel_ID', $lvls_arr)
                ->where('Position_ID', $pos_id)
                ->first();

            if(count($exsisted_level) > 0){
                if($exsisted_level->SkillLevel_ID != $target_lvl_id){
                    DB::table('SMPositions')
                        ->where('ID', $exsisted_level->ID)
                        ->update(['SkillLevel_ID' => $target_lvl_id]);
                }
            }else{
                DB::table('SMPositions')->insert([
                    'Position_ID' => $pos_id, 'SkillLevel_ID' => $target_lvl_id
                ]);
            }
        }

        return redirect("adm_positionprofile/$pos_id");
    }

    public function removeuserskill($user_id, $skill_id){
        //id всех уровней скила
        $lvls = $this->repository->get_skill_levels($skill_id);

        if(count($lvls)>0){
            $this->repository->del_user_skill_levels($user_id, $lvls);
        }

        return redirect("rise/$user_id");
    }

    public function remove_position_skill($pos_id, $skill_id){
        if(!LdapAuth::hasRole('admin')){
            return redirect('/')->with('error', 'Недостаточно прав');
        }

        //id всех уровней скила
        $lvls = $this->repository->get_skill_levels($skill_id);

        if(count($lvls)>0){
            $this->repository->remove_position_skill($pos_id, $lvls);
        }

        return redirect("adm_positionprofile/$pos_id");
    }

    public function skill_rework(Request $request){
        if(LdapAuth::authorize('open_moderate')){
            $skill_id = $request->input('skill_id');
            $comment = $request->input('comment');

            DB::table('Moderating')->insert([
                'StatusID' => 5, 'TableName' => 'Skills', 'PK_ID' => $skill_id, 'UserName' => LdapAuth::user()->employeeid, 'Description' => $comment, 'Date' => new \DateTime()
            ]);
            DB::table('Skills')->where('ID', $skill_id)->update(['Status' => 5]);
        }else{
            return redirect('/')->with('error', 'Недостаточно прав');
        }

        return redirect('moderator');
    }

//    public function skill_toapprove(Request $request){
//        $skill_id = $request->input('skill_id');
//        if(LdapAuth::authorize('update_skill', $skill_id)) {
//            $your_comment = $request->input('your_comment');
//            DB::table('Moderating')->insert([
//                'StatusID' => 1, 'TableName' => 'Skills', 'PK_ID' => $skill_id, 'UserName' => LdapAuth::user()->employeeid, 'Description' => $your_comment, 'Date' => new \DateTime()
//            ]);
//            DB::table('Skills')->where('ID', $skill_id)->update(['Status' => 1]);
//        }else{
//            return redirect('/')->with('error', 'Недостаточно прав');
//        }
//        return redirect('skills');
//    }

    public function skillstorework(){
        $data['title'] = 'Компетенции на доработке';
        $data['Skills'] = $this->repository->get_skills_torework();
        return view('skillstorework', $data);
    }

    public function skillsdraft(){
        $data['title'] = 'Черновики компетенций';
        $data['Skills'] = $this->repository->get_skills_draft();
        return view('skillstorework', $data);
    }

    public function activate_skill($id, News_lib $news_lib){
        if(LdapAuth::authorize('open_moderate')){
            $old_status = DB::table('Skills')->where('ID', $id)->Value('Status');
            $this->repository->set_skill_status($id, 'Активная');

            if($old_status == 1){
                //Активация новой к-ии, а не правки старой
                $name = DB::table('Skills')->where('ID', $id)->Value('Name');
                $link = "<a href='/viewskill/".$id."'>$name</a>";
                $news_lib->add_News(['Owner'=>0, 'Type' => 'SKILL', 'Body' => "Добавлена новая компетенция - $link"]);
            }


            DB::table('Moderating')->insert([
                'StatusID' => 2, 'TableName' => 'Skills', 'PK_ID' => $id, 'UserName' => LdapAuth::user()->employeeid, 'Description' => 'Компетенция утверждена', 'Date' => new \DateTime()
            ]);
            return redirect('/moderator');
        }else{
            return redirect('/');
        }
    }

    public function movetoarhiv($skill_id){
        if(LdapAuth::authorize('move_skill_to_arhiv', $skill_id)){
//            $lvls = $this->repository->get_skill_levels($skill_id);
//            $this->repository->arhiv_clear_staffprogress($lvls); пусть останутся, для истории или оживления скила
            $this->repository->set_skill_status($skill_id, 'Архив');
        }
        return redirect('/skills');
    }
    
}