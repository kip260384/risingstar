<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

use App\Repositories\EngineerRepository;
use App\Http\Controllers\CatalogController;

use App\Facades\LdapAuth;

class ClassName extends BaseController
{
    
    function __construct($argument)
    {
        # code...
    }

    public function insertEmployeeID()
    {
        $userRoles = DB::table('StarMapRights')->select('StarMapRights.ID', 'StarMapRights.SMRolesID', 'StarMapRights.FIO as rightFIO', 'StaffRCS.FIO as staffFIO', 'StaffRCS.EmployeeID')->where('StarMapRights.FIO', '<>', 'null')->join('StaffRCS', 'StarMapRights.FIO', '=', 'StaffRCS.FIO')->get();

        foreach ($userRoles as $role) {
            $EmployeeID = $role->EmployeeID;
            DB::table('StarMapRights')
                ->where('id', $role->ID)
                ->update([
                    "EmployeeID" => $EmployeeID
                ]);
            echo $role->EmployeeID . '<br>';
        }

        $userRoles2 = DB::table('StarMapRights')->select('*')->where('EmployeeID', '<>', null)->get();

        dd($userRoles2);

        // dd(DB::table('StarMapRoles')->select('*')->get());
    }
}