<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Http\Request;
use LdapAuth;

class Admin
{

    public function __construct()
    {
        
    }

    public function handle(Request $request, Closure $next)
    {

        if (LdapAuth::hasRole('admin')) { 
            return $next($request);
        }

        return abort(401);
    }
}