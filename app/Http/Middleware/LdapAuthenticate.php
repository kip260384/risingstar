<?php

namespace App\Http\Middleware;

use Closure;

use App\Facades\LdapAuth;
use Illuminate\Http\Request;

class LdapAuthenticate
{

    public function __construct(LdapAuth $ldap)
    {
        $this->ldap = $ldap;
    }

    public function handle(Request $request, Closure $next)
    {
        //До запуска оставляем доступ к порталу только для участников команды
        $allowedpeople = array(
            'regions\U_3214', 'regions\U_32107', 'regions\u_32ma6',
            'regions\U_020BX', 'regions\U_uem8r',
            'U_15mxv', 'regions\U_23004',
            'moscow\U_00b27'
        );

//        if(
//            !\App\Facades\LdapAuth::hasRole('admin')
//            &&
//            !\App\Facades\LdapAuth::hasRole('manager')
//            &&
//            !\App\Facades\LdapAuth::hasRole('moderator')
//            &&
//            !in_array(\App\Facades\LdapAuth::user()->uname, $allowedpeople)){
//            echo("<img src='/img/soon.jpg' style='width: 100%; height: 100%'>");
//            die();
//        }

        LdapAuth::log();
        $response = $next($request);

//        session()->flush();

        session()->forget('roles');

        // roles = ['admin' => [...], 'manager' => [...], ...]

        session([
            'roles' => LdapAuth::getUserRoles()
        ]);

        return $response;
    }
}