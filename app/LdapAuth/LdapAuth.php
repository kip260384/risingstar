<?php

namespace App\LdapAuth;

use App\Http\Controllers\LdapController;
use App\LdapAuth\Policies;

use DB;
use Route;
use Illuminate\Http\Request;

/**
* 
*/

class LdapAuth
{
    protected $user;

    public function __construct(LdapController $ldap, Policies $policy)
    {
        $this->ldap = $ldap;
        $this->policy = $policy;
        $this->user = $ldap->getUserInfo();
    }

    public function test()
    {
        return $this->managerRights;
    }

    public function log()
    {
        $user = $this->user;
        // save logon stats...
        if (! in_array('api', \Request::segments()) && \Request::root() == 'http://rising-star') {
            if (! isset($user['domain'])) { $user['domain'] = 'nodomain'; }
            if (! isset($user['login'])) { $user['login'] = 'nologin'; }
            DB::table('LogonStats')->insert(
                [
                    'UserAD' => $user['domain'] . '\\' . $user['login'], 
                    'Url' => \Request::url()
                ]
            );
        }
    }

    public function authorize($action, $model=0)
    {
        $user = $this->user;
        
        if ($this->hasRole('admin')) {
            return true;
        }

        return $this->policy->check($user, $action, $model);
    }

    public function hasRole($role)
    {
        return $this->policy->checkRole($this->user, $role);
    }

    public function getUserRoles()
    {
        return $this->policy->getRoles($this->user);
    }

    public function user()
    {
        return (object)$this->user;
    }

    public function userbyad($uname)
    {
        return (object)$this->ldap->getDataAD($uname);
    }
}
