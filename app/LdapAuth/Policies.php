<?php

namespace App\LdapAuth;

use DB;

class Policies
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    protected $roles = ['admin' => 3, 'moderator' => 4, 'manager' => 1, 'expert' => 2];

    //

    protected $rights = [
        4 => ['del_news'], //admin
        3 => ['open_moderate', 'view_skill', 'move_skill_to_arhiv'], //moderator
        2 => [], //expert
        1 => ['update_skill', 'add_user_skill', 'update_user_skill', 'move_skill_to_arhiv'] //manager
    ];

    public function __construct()
    {
        //
    }

    public function haveRights($action, $roleNumber)
    {
        return in_array($action, $this->rights[$roleNumber]);
    }

    public function check($user, $action, $model)
    {
        $policy = new Policies;

        if (method_exists($policy, $action)) {
            return $this->$action($user, $model);
        } else {
            return 'no method find';
        }
    }

    public function getRoles($user)
    {
        $userDBRole = DB::table('StarMapRights')->select('*')->where('EmployeeID', '=', $user['employeeid'])->get();
        $reverse_roles = array_flip($this->roles);
        $userRoles = [];
        foreach ($userDBRole as $role) {
            if(isset($reverse_roles[$role->SMRolesID])){
                $userRoles[$reverse_roles[$role->SMRolesID]][] = $role->SMElementID;
            }
        }

        return $userRoles;
    }

    public function checkRole($user, $role)
    {
        if (array_key_exists($role, $this->roles)) {
            // obtain roles from DB and check
            $userDBRole = DB::table('StarMapRights')->select('*')
                ->where('EmployeeID', '=', $user['employeeid'])
                ->where('SMRolesID', $this->roles[$role])->get();
        } else {
            return false;
        }

        return count($userDBRole) > 0;
    }

    public function update($user, $model)
    {
        //get status
        $status = DB::table('Skills')->where('ID', $model)->value('Status');
        if($status == 2){
            return true;
        }else{
            return $this->checkRole($user, 'expert') or $this->checkRole($user, 'moderator') or $this->checkRole($user, 'manager');
        }
        return false;
    }

    public function create_skill($user, $model)
    {
        return true;
    }

    public function view_skill($user, $model){
        //get status
        $status = DB::table('Skills')->where('ID', $model)->value('Status');
        if($status == 2){
            return true;
        }else{
            return $this->checkRole($user, 'expert') or $this->checkRole($user, 'moderator') or $this->checkRole($user, 'manager');
        }
        return false;
    }

    public function update_skill($user, $model)
    {
        // get all rights for current skill and check user
        $rights_current_model = DB::table('StarMapRights')->select('*')->where('SMElementID', $model)->where('EmployeeID', $user['employeeid'])->get();

        foreach ($rights_current_model as $right) {
            if ($this->haveRights('update_skill', $right->SMRolesID)) {
                return true;
            }
        }

        return false;
    }

    public function delete_skill($user, $model)
    {
        return false;
    }

    public function add_user_skill($user, $model)
    {
        return true;
    }

    public function update_user_skill($user, $model) //$model == skill_id ?
    {
        // get all rights for current skill and check user
        $rights_current_model = DB::table('StarMapRights')->select('*')->where('SMElementID', $model)->where('EmployeeID', $user['employeeid'])->get();

        foreach ($rights_current_model as $right) {
            if ($this->haveRights('update_user_skill', $right->SMRolesID)) {
                return true;
            }
        }

        return false;
    }

    public function delete_user_skill($user, $model)
    {
        // get all right for current skill and check user
        $rights_current_model = DB::table('StarMapRights')->select('*')->where('SMElementID', $model)->where('EmployeeID', $user['employeeid'])->get();

        foreach ($rights_current_model as $right) {
            if ($this->haveRights('delete_user_skill', $right->SMRolesID)) {
                return true;
            }
        }

        return false;
    }

    public function open_moderate($user, $model){
        return $this->checkRole($user, 'moderator');
    }

    public function move_skill_to_arhiv($user, $model){
        // get all rights for current skill and check user
        $rights_current_model = DB::table('StarMapRights')->select('*')->where('SMElementID', $model)->where('EmployeeID', $user['employeeid'])->get();

        foreach ($rights_current_model as $right) {
            if ($this->haveRights('move_skill_to_arhiv', $right->SMRolesID)) {
                return true;
            }
        }

        return false;


    }

}
