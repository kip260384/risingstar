<?php
namespace App\Libraries;

use DB;
use App\Repositories\CatalogRepository;

class Catalog_lib
{
    public function __construct(CatalogRepository $repository)
    {
        $this->repository = $repository;
    }


    function isParent($catalog, $id)
    {
        foreach ($catalog as $cat) {
            if ($cat->Parent_ID == $id) {
                return true;
            }
        }
        return false;
    }

    public function getcatalog_arr(){
        $tree = array();
        $tree[] = array("Value" => 0, "DisplayText" => "Нет родителя");
        $catalog = $this->repository->getcatalog();

        $this->buildCatalog_arr(0, $tree, $catalog);

        return $tree;
    }

    function buildCatalog_arr($parent, &$tree, &$catalog = 0, $tab=''){
        foreach($catalog as $row){
            if($row->Parent_ID == $parent){
                $row->Cat_name = $tab.$row->Cat_name;

                $tree[] = array("Value" => $row->Cat_id, "DisplayText" => $row->Cat_name);
                $this->buildCatalog_arr($row->Cat_id, $tree, $catalog, $tab.'---');
            }
        }

        return $tree;
    }

    function getCatalogStr($select_id = 'skill_catalog_tree', $skillsgr_filter = null){
        $str = "<select id='$select_id' name='catalog_tree[]' multiple='multiple' size='30'>";
        $str .= $this->buildCatalogStr(0, 0, '', $skillsgr_filter);
        $str .= "</select>";
        return $str;
    }

    function getSkillsStr($select_id = 'skills_tree', $skills_filter = null){
        $str = "<select id='$select_id' name='catalog_tree[]' multiple='multiple' size='30'>";
        $str .= $this->buildSkillsStr(0, 0, '', $skills_filter);
        $str .= "</select>";

        return $str;
    }

    function getManagerSkillsStr($select_id = 'skills_tree', $skills_filter = null){
        $str = "<select id='$select_id' name='catalog_tree[]' multiple='multiple' size='30'>";
        $str .= $this->buildSkillsStr(0, $this->repository->getCatalogForManager(), '', $skills_filter);
        $str .= "</select>";
        return $str;
    }

    function buildCatalogStr($parent, $catalog = 0, $p_name='', $skillsgr_filter = null){
        if($catalog === 0){
            $catalog = $this->repository->getcatalog();

            if(count($catalog)>0){
                $p_name = 'Каталог';
            }
        }
        $tree = '';

        foreach($catalog as $row){
            if($row->Parent_ID == $parent){
                if(!$this->isParent($catalog, $row->Cat_id)){//jQ Скрипт сам создаёт родительскую строку, поэтому тут приходится пропускать
                    $sel = '';
                    if(is_array($skillsgr_filter)){
                        foreach ($skillsgr_filter as $key => $value){
                            if($value == $row->Cat_id){
                                $sel = "selected='selected'";
                            }
                        }
                    }
                    $tree .= "<option value='$row->Cat_id' data-section='$p_name' $sel>$row->Cat_name</option>";
                }
                $tree .= $this->buildCatalogStr($row->Cat_id, $catalog, $p_name.'/'.$row->Cat_name, $skillsgr_filter);
            }
        }
        return $tree;
    }

    function buildSkillsStr($parent, $catalog = 0, $p_name='', $skills_filter = null){
        if($catalog === 0){
            $catalog = $this->repository->getcatalog()->toArray();

            $skills = DB::table('Skills')->select('ID as SkillID', 'Name', 'SkillsGroupID as Parent_ID')
                ->where('Status', 2)
                ->get()
                ->toArray();

            $catalog = array_merge($catalog, $skills);

            if(count($catalog)>0){
                $p_name = 'Каталог';
            }
        }
        $tree = '';
        foreach($catalog as $row){
            if($row->Parent_ID == $parent){
                if(isset($row->Cat_id)){
                    if(!$this->isParent($catalog, $row->Cat_id)){//jQ Скрипт сам создаёт родительскую строку, поэтому тут приходится пропускать
//                        $tree .= "<option value='-1' data-section='$p_name' disabled='disabled'>$row->Cat_name</option>";
                    }
                    $tree .= $this->buildSkillsStr($row->Cat_id, $catalog, $p_name.'/'.$row->Cat_name, $skills_filter);
                }else{
                    $sel = '';
                    if(is_array($skills_filter)){
                        foreach ($skills_filter as $key => $value){
                            if($value == $row->SkillID){
                                $sel = "selected='selected'";
                            }
                        }
                    }

                    $tree .= "<option value='$row->SkillID' data-section='$p_name' $sel>$row->Name</option>";
                }
            }
        }
        return $tree;
    }


}