<?php
namespace App\Libraries;

use DB;

class Dashboard_lib
{
    public function __construct()
    {
        //
    }

    public function get_pr_appraised($city = null)
    {
        //Общее число
        $total = DB::table('StaffRCS')->count();
        if ($total === 0) {
            return 0;
        }
        //Число с оценками
        //За оцененных считаем людей хотябы с одним скилом уровнем > 0
        $appraised_q = DB::table('StaffRCS')
            ->select('StaffRCS.EmployeeID')
            ->rightjoin('StaffProgress', 'StaffRCS.EmployeeID', '=', 'StaffProgress.Staff_ID')
            ->join('SkillLevels', 'StaffProgress.SkillLevels_ID', '=', 'SkillLevels.ID')
            ->where('SkillLevels.SkillLevel', '>', 0)
            ->groupBy('StaffRCS.EmployeeID')
            ->get();

        $appraised = count($appraised_q);

        return ($appraised / $total) * 100;
    }

    public function get_experts_number(){
        $ex_role_id = DB::table('StarMapRoles')->where('Name', 'Эксперт')->value('ID');
        $experts = DB::table('StarMapRights')->select('EmployeeID')
            ->where('SMRolesID', $ex_role_id)
            ->groupBy('EmployeeID')
            ->get();

        return count($experts);
    }

    public function get_rcs_number(){
        return DB::table('StaffRCS')->count();
    }

    public function get_starsCount(){
        $stars = DB::table('StaffProgress')->select('SkillLevel')
            ->join('SkillLevels', 'SkillLevels.ID', '=', 'StaffProgress.SkillLevels_ID')
            ->sum('SkillLevel');

        return $stars;

    }

}