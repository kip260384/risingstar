<?php

namespace App\Libraries;

use App\News_feed;

class News_lib
{
    public function add_News($data)
    {
        $news = new News_feed;
        $news->Owner = $data['Owner'];
        $news->Type = $data['Type'];
        $news->Body = $data['Body'];

        $news->save();
    }
}