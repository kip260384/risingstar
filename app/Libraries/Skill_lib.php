<?php
namespace App\Libraries;

use DB;

class Skill_lib
{
    public function __construct()
    {

    }

    function buildChildren($catalog, $parent)
    {
        $out = [];
        if (!isset($catalog[$parent])) {
            return $out;
        }
        foreach ($catalog[$parent] as $row) {
            $childs = $this->buildChildren($catalog, $row['GroupID']);
            if ($childs) {
                $row['children'] = $childs;
            }
            $out[] = $row;
        }

        return $out;
    }
}