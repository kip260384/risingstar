<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
* 
*/
class LdapAuthServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('LdapAuth', 'App\LdapAuth\LdapAuth');
    }

}