<?php
/**
 * Created by PhpStorm.
 * User: U_32107
 * Date: 19.04.2017
 * Time: 11:04
 */

namespace App\Repositories;

use DB;

class EngineerRepository
{

    private $allow_filter_keys = ['city', 'skill'];

    public function getAll()
    {
        $all = DB::table('StaffRCS')->select('*')->orderBy('FIO')->get();

        return $all;
    }

    public function getAllCities()
    {
        $cities = DB::table('StaffRCS')->select('city')->distinct()->get();

        return $cities;
    }

    public function get_WishedCities($Employee_ID){
        return DB::table('WishedCities')->select('City_name')
            ->where('Employee_ID', $Employee_ID)
            ->get();
    }

    public function getWishedPosition($Employee_ID){
        $wPos = DB::table('WishedPositions')
            ->select('Position_ID as ID', 'Positions.Position as Name')
            ->join('Positions', 'Position_ID', '=', 'Positions.ID')
            ->where('Employee_ID', $Employee_ID)
            ->first();

        if($wPos == null){
            $wPos = new \stdClass();
            $wPos->ID = 0;
            $wPos->Name = 'Не выбрано';
        }

        return $wPos;
    }

    public function getPositions($CenterType){
        return DB::table('Positions')->select('*')
            ->where('CenterType', $CenterType)
            ->get();
    }

    public function getByFilter($filter = [])
    {
        $engineers = DB::table('StaffRCS')
            ->select('FIO', 'City', 'StaffRCS.EmployeeID', 'Position');
        if(isset($filter['city']) && $filter['city'] !== ''){
            $engineers->where('City', $filter['city']);
        }
        if(isset($filter['skill']) && count($filter['skill'])>0){
            $skills_filter = array();
            foreach ($filter['skill'] as $skill_id){
                $skills_filter[] = $skill_id;
            }
            session(['skills_filter' => $skills_filter]);

            $engineers->join('StaffProgress', 'StaffRCS.EmployeeID', '=', 'StaffProgress.Staff_ID');
            $engineers->join('SkillLevels', 'StaffProgress.SkillLevels_ID', '=', 'SkillLevels.ID');
            $engineers->where(function ($query) use ($filter){
                foreach ($filter['skill'] as $skill_id){
                    $query->orWhere('SkillLevels.Skills_ID', $skill_id);
                }
            });
        }

        return $engineers->orderBy('FIO')->distinct()->get();
    }

    function getFIO($id){
        return DB::table('StaffRCS')->where('EmployeeID', $id)->value('FIO');
    }

    function getAllUserDataByEID($employee_id){
        return DB::table('Staff')->select('*')->where('EmployeeID', $employee_id)->first();
    }

    function add_wishedcity($Employee_ID, $city){
        //Проверить что города ещё нет в списке
        $check = DB::table('WishedCities')->select('*')
            ->where('Employee_ID', $Employee_ID)
            ->where('City_name', $city)
            ->get();

        if(count($check)==0){
            DB::table('WishedCities')->insert([
                'Employee_ID' =>  $Employee_ID, 'City_name' => $city
            ]);
        }

        return;
    }

    function del_wishedcity($Employee_ID, $city){
        DB::table('WishedCities')
            ->where('Employee_ID', $Employee_ID)
            ->where('City_name', $city)
            ->delete();

        return;
    }

    function set_WishedPosition($Employee_ID, $Position){
        DB::table('WishedPositions')->where('Employee_ID', $Employee_ID)->delete();
        if($Position > 0){
            DB::table('WishedPositions')->insert([
                'Employee_ID' => $Employee_ID, 'Position_ID' => $Position
            ]);
        }

        return;
    }

}