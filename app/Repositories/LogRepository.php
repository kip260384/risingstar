<?php

namespace App\Repositories;

use DB;
use \Carbon\Carbon;

class LogRepository
{
    public function __construct()
    {

    }

    public function getVisits()
    {
        $all = DB::table('LogonStats')
            ->leftJoin('Staff', 'LogonStats.UserAD', '=', 'Staff.ADName')
            ->orderBy('createdat')
            ->get();

        $grouped = $all->transform(function($item, $key) {
            $item->day = date_format(date_create($item->createdat), 'Y-m-d');
            return $item;
        });

        $grouped = $grouped->groupBy('day');

        foreach ($grouped as $key => $value) {
            $grouped[$key] = $value->groupBy('UserAD');
        }

        return $grouped;
    }

    public function gropByDay($arr = [])
    {
        $grouped = $all->transform(function($item, $key) {
            $item->day = date_format(date_create($item->createdat), 'd.m.Y');
            return $item;
        });
        
        return $grouped;
    }

    public function getVisitsByPeriod($period = [])
    {
        $all = DB::table('LogonStats')->get();

        $grouped = $all->groupBy('createdat');

        $grouped = $all->transform(function($item, $key) {
            $item->day = date_format(date_create($item->createdat), 'd.m.Y');
            return $item;
        });

        $grouped = $grouped->groupBy('day');

        return $grouped;
    }
}