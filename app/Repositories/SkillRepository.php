<?php

namespace App\Repositories;

use DB;
use App\Libraries\Skill_lib;
use App\Facades\LdapAuth;

class SkillRepository
{
    public function __construct(Skill_lib $skill_lib)
    {
        $this->skill_lib = $skill_lib;
    }

    public function getAll()
    {
        $all = DB::table('Skills')->get();

        return $all;
    }

    public function getSkillsByHierarchy()
    {
        $skillGroup = DB::table('SkillsGroup')
                ->leftJoin('Skills', 'SkillsGroup.ID', '=', 'Skills.SkillsGroupID')
                ->leftJoin('SkillLevels', 'Skills.ID', '=', 'SkillLevels.Skills_ID')
                ->leftJoin('StaffProgress', 'SkillLevels.ID', '=', 'StaffProgress.SkillLevels_ID')
                ->select(DB::raw('Skills.ID as SkillId, SkillsGroup.ID as GroupId, SkillsGroup.Name as GroupName, Skills.Name as SkillName, SkillsGroup.Parent_ID as GroupParent, count(StaffProgress.Staff_ID) as countRised'))
                ->groupBy('SkillsGroup.ID')
                ->groupBy('SkillsGroup.Parent_ID')
                ->groupBy('Skills.ID')
                ->groupBy('SkillsGroup.Name')
                ->groupBy('Skills.Name')
                ->get();

        // dd($skillGroup);

        $skillGroupJson = [];
        $i = 0;

        foreach ($skillGroup as $group) {
            $skillGroupJson[$group->GroupParent][$i] = [
                'ID' => $group->SkillId,
                'GroupID' => $group->GroupId,
                'Parent_ID' => $group->GroupParent,
                'name' => isset($group->SkillName) ? $group->SkillName : $group->GroupName
            ];

            if (isset($group->SkillId) && ($group->countRised > 0)) {
                $skillGroupJson[$group->GroupParent][$i]['size'] = $group->countRised;
            }

            $i++;
        }

        // dd($skillGroupJson);

        $json = $this->skill_lib->buildChildren($skillGroupJson, 0);

        return $json;

    }

    public function get_moderating_history($skill_id){
        return DB::table('Moderating')->select('Description', 'FIO', 'Date')
            ->join('Staff', 'UserName', '=', 'Staff.EmployeeID')
            ->where('TableName', 'Skills')
            ->where('PK_ID', $skill_id)
            ->get();
    }

    //Достаю менеджера и эксперта
    public function get_skill_manex($skill_id){
        return DB::table('StarMapRights')
            ->select('SMRolesID', 'Staff.FIO as FIO', 'Staff.ADName as ADName', 'StarMapRights.EmployeeID as EmployeeID')
            ->join('Staff', 'Staff.EmployeeID', '=', 'StarMapRights.EmployeeID')
            ->where('SMElementID', $skill_id)
            ->get();
    }

    public function get_skill_status($skill_id){
        return DB::table('Skills')->where('ID', $skill_id)->value('Status');
    }

    public function update_skill($skill_id, $data){
        DB::table('Skills')
            ->where("ID", $skill_id)
            ->update($data);
    }

    public function insert_skill($data){
        return DB::table('Skills')->insertGetId($data);
    }

    public function update_skill_manex($skill_id, $manager_id, $expert_id){
        DB::table('StarMapRights')
            ->where([
                ['SMRolesID' , 1],
                ['SMElementID', $skill_id]
            ])
            ->update([
                "EmployeeID" => $manager_id
            ]);

        DB::table('StarMapRights')
            ->where([
                ['SMRolesID' , 2],
                ['SMElementID', $skill_id]
            ])
            ->update([
                "EmployeeID" => $expert_id
            ]);
    }

    public function update_skill_levels_data($skill_id, $data){
        DB::table('SkillLevels')
            ->where([
                ['Skills_ID', $skill_id],
                ['SkillLevel', 1]
            ])
            ->update(['Description' => $data['desc1'], 'Test' => $data['test1']]);
        DB::table('SkillLevels')
            ->where([
                ['Skills_ID', $skill_id],
                ['SkillLevel', 2]
            ])
            ->update(['Description' => $data['desc2'], 'Test' => $data['test2']]);
        DB::table('SkillLevels')
            ->where([
                ['Skills_ID', $skill_id],
                ['SkillLevel', 3]
            ])
            ->update(['Description' => $data['desc3'], 'Test' => $data['test3']]);
    }

    public function add_skill_manex($skill_id, $manager_id, $expert_id){
        DB::table('StarMapRights')->insert([
            ['SMRolesID' => 1, 'SMElementID' => $skill_id, 'EmployeeID' => $manager_id],
            ['SMRolesID' => 2, 'SMElementID' => $skill_id, 'EmployeeID' => $expert_id]
        ]);
    }

    public function add_skill_levels_data($skill_id, $data){
        DB::table('SkillLevels')->insert([
            ['Skills_ID' => $skill_id, 'SkillLevel' => 0, 'Description' => "", 'Test' => ''],
            ['Skills_ID' => $skill_id, 'SkillLevel' => 1, 'Description' => $data['desc1'], 'Test' => $data['test1']],
            ['Skills_ID' => $skill_id, 'SkillLevel' => 2, 'Description' => $data['desc2'], 'Test' => $data['test2']],
            ['Skills_ID' => $skill_id, 'SkillLevel' => 3, 'Description' => $data['desc3'], 'Test' => $data['test3']]
        ]);
    }

    public function get_skill($skill_id){
        return DB::table('Skills')
            ->join('SkillsGroup', 'SkillsGroup.ID', '=', 'Skills.SkillsGroupID')
            ->join('Status', 'Skills.Status', '=', 'Status.ID')
            ->select('Skills.ID', 'Skills.Name as SkillName', 'Skills.Description as SkillDesc', 'SkillsGroup.Name as GroupName', 'Position', 'Materials', 'Status', 'Status.Name as StatusName')
            ->where('Skills.ID', $skill_id)
            ->first();
    }

    public function get_skill_levels_data($skill_id){
        return DB::table('SkillLevels')
            ->select('SkillLevel', 'Description', 'Test')
            ->where('Skills_ID', $skill_id)
            ->get();
    }

    public function get_active_skills_by_cats($cat_arr){
        $skills =  DB::table('Skills')
            ->select('*');

        if (count($cat_arr) > 0) {
            $skills->whereIn('SkillsGroupID', $cat_arr);
        }

        $skills = $skills->where('Status', 2) //2 - статус активная
            ->get();

        //Удаляет разметку, Выбирает первые n символов
        foreach($skills as &$skill){
            $str = strip_tags($skill->Description, '<br>');
            $skill->Description = $str;
        }

        return $skills;
    }

    public function get_full_cat_path($skill_id){
        $parent = DB::table('Skills')->where('ID',$skill_id)->value('SkillsGroupID');

        $path_arr = array();
        do{
            $item = DB::table('SkillsGroup')->where('ID', $parent)->value('Name');
            array_unshift($path_arr, $item); //Добавляет элемент в начало массива
            $parent = DB::table('SkillsGroup')->where('ID', $parent)->value('Parent_ID');
        }while($parent > 0);

        $str = '';
        foreach ($path_arr as $path){
            $str .= $path.'/';
        }

        $str = substr_replace($str,"",-1); //Убирает последний символ

        return $str;
    }

    function collect_skill_data($skills){
        $result = array();
        $n=0;
        foreach ($skills as $skill){
            $result[$n] = array(
                'id' => $skill->ID,
                'name' => $skill->Name,
                'desc' => $skill->Description,
                'fullpath' => $this->get_full_cat_path($skill->ID),
                'manager_fio' => '',
                'manager_id' => '',
                'expert_fio' => '',
                'expert_id' => ''
            );
            //Достаю менеджера и эксперта
            $men = $this->get_skill_manex($skill->ID);

            foreach ($men as $man){
                if($man->SMRolesID == 1){
                    $result[$n]['manager_fio'] .= $man->FIO;
                    $result[$n]['manager_id'] .= $man->EmployeeID;
                }
                if($man->SMRolesID == 2){
                    $result[$n]['expert_fio'] .= $man->FIO;
                    $result[$n]['expert_id'] .= $man->EmployeeID;
                }
            }

            $n++;
        }

        return $result;
    }

    public function get_user_skills($u_id){
        $skills = DB::table('StaffProgress')
            ->select('SkillLevels_ID', 'SkillLevels.Skills_ID as SkillID', 'Skills.Name as SkillName', 'SkillLevels.SkillLevel as SkillLevel')
            ->join('SkillLevels', 'SkillLevels_ID', '=', 'SkillLevels.ID')
            ->join('Skills', 'SkillLevels.Skills_ID', '=', 'Skills.ID')
            ->where('Staff_ID', $u_id)
            ->get();

        return $skills;
    }

    function get_skilllevel_for_user($skill_id, $employee_id){
        $level = DB::table('StaffProgress')
            ->select('SkillLevel')
            ->join('SkillLevels', 'SkillLevels.ID', '=', 'StaffProgress.SkillLevels_ID')
            ->where('Staff_ID', $employee_id)
            ->where('Skills_ID', $skill_id)
            ->first();

        if(count($level) == 0){
            return 0;
        }else{
            return $level->SkillLevel;
        }

    }

    public function get_position_skills($position_id){
        return DB::table('SMPositions')
            ->join('SkillLevels', 'SMPositions.SkillLevel_ID', '=', 'SkillLevels.ID')
            ->join('Skills', 'SkillLevels.Skills_ID', '=', 'Skills.ID')
            ->select('Skills.ID as SkillID', 'Skills.Name as SkillName', 'SkillLevel')
            ->where('Position_ID', $position_id)
            ->get();
    }

    public function get_new_skills(){
        $skills = DB::table('Skills')->select('*')->where('Status', 1)->get();
        $result = $this->collect_skill_data($skills);
        return $result;
    }

    public function get_skills_torework(){
        $skills = DB::table('Skills')
            ->select('Skills.Name as Name', 'Skills.ID as ID', 'Description')
            ->join('StarMapRights', 'SMElementID', '=', 'Skills.ID')
            ->where('SMRolesID', 1)
            ->where('EmployeeID', LdapAuth::user()->employeeid)
            ->where('Status', 5)
            ->get();

        if(count($skills) == 0){
            return false;
        }

        $result = $this->collect_skill_data($skills);
        return $result;
    }

    public function get_null_level_for_skill($skill_id){
        return DB::table('SkillLevels')
            ->where('Skills_ID', $skill_id)
            ->where('SkillLevel', 0)
            ->value('ID');
    }

    public function get_skill_levels($skill_id){
        return DB::table('SkillLevels')
            ->where('Skills_ID', $skill_id)
            ->pluck("ID");
    }

    public function del_user_skill_levels($user_id, $lvls){
        DB::table('StaffProgress')
            ->where('Staff_ID', $user_id)
            ->whereIn('SkillLevels_ID', $lvls)
            ->delete();
    }

    public function remove_position_skill($pos_id, $lvls){
        DB::table('SMPositions')
            ->where('Position_ID', $pos_id)
            ->whereIn('SkillLevel_ID', $lvls)
            ->delete();
    }

    function get_skills_draft(){
        $skills = DB::table('Skills')
            ->select('Skills.Name as Name', 'Skills.ID as ID', 'Description')
            ->join('StarMapRights', 'SMElementID', '=', 'Skills.ID')
            ->where('SMRolesID', 1)
            ->where('EmployeeID', LdapAuth::user()->employeeid)
            ->where('Status', 6)
            ->get();

        if(count($skills) == 0){
            return false;
        }

        $result = $this->collect_skill_data($skills);
        return $result;
    }

    public function get_skill_levels_ids($skill_id){
        return DB::table('SkillLevels')->select("ID", 'SkillLevel')
            ->where('Skills_ID', $skill_id)
            ->get();
    }

    public function arhiv_clear_staffprogress($lvls_ids_arr){
        DB::table('StaffProgress')
            ->whereIn('SkillLevels_ID', $lvls_ids_arr)
            ->delete();
    }

    public function set_skill_status($skill_id, $status){
        $new_status_id = DB::table('Status')->where('Name', $status)->value('ID');

        if($new_status_id > 0){
            DB::table('Skills')->where('ID', $skill_id)->update(['Status' => $new_status_id]);
        }else{
            return false;
        }
    }

}