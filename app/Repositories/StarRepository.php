<?php
/**
 * Created by PhpStorm.
 * User: U_32107
 * Date: 19.04.2017
 * Time: 11:04
 */

namespace App\Repositories;

use DB;

class StarRepository
{

    public function getAllStars()
    {
        $all = DB::table('StaffProgress')
            ->join('SkillLevels', 'StaffProgress.SkillLevels_ID', '=', 'SkillLevels.ID')
            ->select('SkillLevels.Skills_ID', 'SkillLevels.SkillLevel', 'StaffProgress.Staff_ID', 'StaffProgress.SkillLevels_DT')
            ->get();

        return $all;
    }

    public function getStarsById($employee_id)
    {
        $stars = DB::table('StaffProgress')
            ->join('SkillLevels', 'StaffProgress.SkillLevels_ID', '=', 'SkillLevels.ID')
            ->join('Skills', 'SkillLevels.Skills_ID', '=', 'Skills.ID')
            ->select('Skills.ID', 'Skills.Name', 'SkillLevels.SkillLevel', 'StaffProgress.Staff_ID')
            ->where('Staff_ID', $employee_id)
            ->get();

        return $stars;
    }

    public function getStarsByPosition($position)
    {
        is_numeric($position) ? $position_id = $position : $position_id = DB::table('Positions')->where('Position', $position)->value('ID');

        // dd($position_id);

        $stars = DB::table('SMPositions')
            ->join('SkillLevels', 'SMPositions.SkillLevel_ID', '=', 'SkillLevels.ID')
            ->join('Skills', 'SkillLevels.Skills_ID', '=', 'Skills.ID')
            ->select('Skills.ID', 'Skills.Name', 'SkillLevels.SkillLevel as NeedLevel')
            ->where('Position_ID', $position_id)
            ->get();

        // dd(count($stars));

        if (count($stars) === 0) {
            return [
                // ["ID" => 9, "Name" => ".NET", "NeedLevel" => 3],
                // ["ID" => 10, "Name" => "Java", "NeedLevel" => 2],
                // ["ID" => 51, "Name" => "Администрирование Windows систем", "NeedLevel" => 3],
                // ["ID" => 52, "Name" => "Наставник", "NeedLevel" => 3],
                // ["ID" => 53, "Name" => "Наличие наставника", "NeedLevel" => 2],
                // ["ID" => 55, "Name" => "Сопровождение СКС", "NeedLevel" => 3],
                // ["ID" => 56, "Name" => "Ведение переговоров", "NeedLevel" => 2],
                // ["ID" => 57, "Name" => "Публичные выступления", "NeedLevel" => 2],
                // ["ID" => 15, "Name" => "Периферия", "NeedLevel" => 2],
                // ["ID" => 26, "Name" => "Готовность к релокации", "NeedLevel" => 2],
                // ["ID" => 35, "Name" => "Стратегические проекты ДРО", "NeedLevel" => 2],
                // ["ID" => 38, "Name" => "Сопровождение ПТС", "NeedLevel" => 2],
                // ["ID" => 45, "Name" => "Английский", "NeedLevel" => 2],
                // ["ID" => 49, "Name" => "ITIL, ITSM", "NeedLevel" => 2]
            ];
        }

        return $stars->toArray();
    }



}