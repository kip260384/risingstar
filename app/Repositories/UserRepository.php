<?php

namespace App\Repositories;

use App\Http\Controllers\LdapController;
use DB;

class UserRepository
{
    public function __construct(LdapController $user)
    {
        $this->user = $user;
    }

    public function getusers($query, $region = '')
    {
        $users = $this->user->getSubstFIO($query, $region);

        return $users;
    }

    public function getPosition($employee_id)
    {
        return DB::table('StaffRCS')->where('EmployeeId', $employee_id)->value('Position');
    }
}