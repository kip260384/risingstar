<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StaffProgress', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Staff_ID');
            $table->integer('SkillLevels_ID');
            $table->dateTime('SkillLevels_DT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('StaffProgress');
    }
}
