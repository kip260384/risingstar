<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SkillLevels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Skills_ID');
            $table->integer('SkillLevel');
            $table->string('Description');
            $table->string('Test');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SkillLevels');
    }
}
