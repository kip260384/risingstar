<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffProgressHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StaffProgress_History', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Staff_ID');
            $table->integer('SkillLevels_ID');
            $table->datetime('SkillLevels_DT');
            $table->datetime('Change_DT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('StaffProgress_History');
    }
}
