<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModeratingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Moderating', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('StatusID');
            $table->string('TableName');
            $table->integer('PK_ID');
            $table->string('UserName');
            $table->string('Description');
            $table->dateTime('Date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Moderating');
    }
}
