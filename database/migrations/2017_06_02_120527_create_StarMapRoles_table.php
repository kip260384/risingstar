<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateStarMapRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StarMapRoles', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('StarMapsID');
            $table->integer('SMObjectID');
            $table->string('Name');
            $table->string('Description');
        });

        DB::table('StarMapRoles')->insert([
            ['ID' => 1, 'StarMapsID' => 1, 'SMObjectID' => 1, 'Name' => 'Менеджер', 'Description' => ""],
            ['ID' => 2, 'StarMapsID' => 1, 'SMObjectID' => 1, 'Name' => 'Эксперт', 'Description' => ""],
            ['ID' => 3, 'StarMapsID' => 1, 'SMObjectID' => 1, 'Name' => 'Администратор', 'Description' => ""],
            ['ID' => 4, 'StarMapsID' => 1, 'SMObjectID' => 1, 'Name' => 'Модератор', 'Description' => ""]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('StarMapRoles');
    }
}
