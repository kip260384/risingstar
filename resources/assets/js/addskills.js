require('./jquery-ui.min');

function autocompleteOptions(url, id) {
  var cache = {};

  return {
    minLength : 3,
      delay: 400,
      maxHeight: 450,
      search: function( event, ui ) {
          $('.spinner').show();
      },
      source: function (query, done) {
          console.log(query);
          var term = query.term;
          if ( term in cache ) {
              $('.spinner').hide();
              return done( cache[ term ] );
          }
          $.ajax({
              url: url,
              dataType: 'json',
              data: {
                  'query': query
              },
              success: function (respone) {
                  console.log('success autocomplete ajax call');
                  console.log(respone);
                  cache[ term ] = respone.data;
                  $('.spinner').hide();
                  done(respone.data);
              },
              error: function (err) {
                  $('.spinner').hide();
                  console.log('error autocomplete ajax call');
                  console.log(err);
              }
          });
      },
      select: function( event, ui ) {
          $('#' + id + '_fio').val( ui.item.displayname );
          $('#' + id + '_id').val( ui.item.employeeid );
          return false;
      }
  }
}

$(document).ready(function() {

  console.log('addskill js...');
    // return;

  $('#manager_fio').autocomplete(autocompleteOptions('/api/v1/getusersbyfio', 'manager')).autocomplete( "instance" )._renderItem = function( ul, item ) {
      var photo = "<img width='48px' height='48px' src='data:image/png;base64, " + item.thumbnailphoto + "' style='float:left'>";
      return $( "<li>" )
      .append( "<div>" + photo + "<div style='margin-left: 58px;'>"  + item.displayname + "<br>" + item.telephonenumber + " | " + item.title + " | " + item.physicaldeliveryofficename + "</div></div>" )
          .appendTo( ul );
  };

  $('#expert_fio').autocomplete(autocompleteOptions('/api/v1/getusersbyfio', 'expert')).autocomplete( "instance" )._renderItem = function( ul, item ) {
      var photo = "<img width='48px' height='48px' src='data:image/png;base64, " + item.thumbnailphoto + "' style='float:left'>";
      return $( "<li>" )
      .append( "<div>" + photo + "<div style='margin-left: 58px;'>"  + item.displayname + "<br>" + item.telephonenumber + " | " + item.title + " | " + item.physicaldeliveryofficename + "</div></div>" )
          .appendTo( ul );
  };

});