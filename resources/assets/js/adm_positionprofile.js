require('./jquery.tree-multiselect');

var params = {
    // sortable: true,
    searchable: true,
    hideSidePanel: true
};

$("select#skills_tree").treeMultiselect(params);