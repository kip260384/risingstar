import * as d3 from 'd3';

var diameter = 960,
    format = d3.format(",d"),
    color = d3.scaleOrdinal(d3.schemeCategory20c);

var bubble = d3.pack()
    .size([diameter, diameter])
    .padding(1.5);

var svg = d3.select("div#skills-bubble").append("svg")
    .attr("viewBox","0 0 960 960")
    .attr("perserveAspectRatio","xMinYMid")
    .attr("width", diameter)
    .attr("height", diameter)
    .attr("class", "bubble");

// var tooltip = d3.select("body")
//     .append("div")
//     .style("position", "absolute")
//     .style("z-index", "10")
//     .style("visibility", "hidden")
//     .style("color", "white")
//     .style("padding", "8px")
//     .style("background-color", "rgba(0, 0, 0, 0.75)")
//     .style("border-radius", "6px")
//     .style("font", "12px sans-serif")
//     .text("tooltip");

d3.json("/api/v1/getallskills", function(error, data) {
// d3.json("flare.json", function(error, data) {
  if (error) throw error;
  // console.log(data);
  var root = d3.hierarchy(classes(data))
      .sum(function(d) { return d.value; })
      .sort(function(a, b) { return b.value - a.value; });

  bubble(root);

  var node = svg.selectAll(".node")
      .data(root.children)
      .enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
      .append("a")
      .attr("xlink:href", function (d) {
        return "/viewskill/" + d.data.id;
      });

  node.append("title")
      .text(function(d) { return d.data.className + ": " + format(d.value); });

  node.append("circle")
      .attr("r", function(d) { return d.r; })
      .style("fill", function(d) { return color(d.data.className); })
      // .on("mouseover", function(d) {
      //   tooltip.text(d.data.className + ": " + format(d.data.value));
      //   tooltip.style("visibility", "visible");
      // })
      // .on("mousemove", function() {
      //   return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
      // })
      // .on("mouseout", function(){return tooltip.style("visibility", "hidden");})
      .on("click", function(d) {
        // console.log('click circle...');
      });

    // node.append("foreignObject")
    //   .attr("width", function(d) {
    //     return (Math.sqrt(2) * d.r);
    //   })
    //   .attr("height", function(d) {
    //     return (Math.sqrt(2) * d.r);
    //   })
    //   .attr("x", function(d) {
    //     return -(Math.sqrt(2) * d.r) / 2;
    //   })
    //   .attr("y", function(d) {
    //     return -(Math.sqrt(2) * d.r) / 2;
    //   })
    //   .append("xhtml:body")
    //     .style("width", "100%")
    //     // .style("position", "initial")
    //     .style("height", "100% !important")
    //     .style("display", "table")
    //     .style("background", "none")
    //     .style("text-align", "center")
    //     .style("line-height", 1)
    //     .html(function(d) {
    //       var words = d.data.className.split(' ');
    //       var longest = '';
    //       var str = '';
    //       for (var i=0; i < words.length; i++) {
    //         str += '<p style="margin:0;">' + words[i] + '</p>';
    //         if (words[i].length > longest.length) {
    //           longest = words[i];
    //         }
    //       }
    //       var fontSize = 2 * ((Math.sqrt(2) * d.r) / longest.length);

    //       return '<div style="font-size:' + Math.round(fontSize) + 'px; display: table-cell; vertical-align: middle; color:#000">' + str + '</div>';
    //     });

  node.append("text")
    // .style("font-size", function(d) {
    //   var words = d.data.className.split(' ');
    //   var longest = '';
    //   for (var i=0; i < words.length; i++) {
    //     if (words[i].length > longest.length) {
    //       longest = words[i];
    //     }
    //   }
    //   var fontSize = 2 * ((Math.sqrt(2) * d.r) / 10);

    //   return Math.round(fontSize);
    // })
    .style("text-anchor", "middle")
    .selectAll("tspan")
    .data(function(d) {
      // var fontSize = 20;
      // if (d.r < 50) {
      //   fontSize = 20;
      // } else if (d.r < 80) {
      //   fontSize = 40;
      // } else if (d.r < 140) {
      //   fontSize = 50;
      // } else if (d.r < 200) {
      //   fontSize = 60;
      // }
      var words = d.data.className.split(' ');
      var longest = '';
      for (var i=0; i < words.length; i++) {
        if (words[i].length > longest.length) {
          longest = words[i];
        }
      }
      var fontSize = 2 * ((Math.sqrt(2) * d.r) / longest.length);
      if (fontSize <= 20) {
        fontSize = 20;
      } else if (fontSize <= 40) {
        fontSize = 30;
      } else {
        fontSize = 50;
      }
      return (d.value > 0) ? words.map(function(item) {return Math.round(d.r) + '__' + fontSize + '__' + item }) : '';
    })
    .enter().append("tspan")
      .style('font-size', function (d) {
        // console.log(d);
        return d.split('__')[1] + 'px';
      })
      .attr("x", 0)
      .attr("y", function(d, i, nodes) {
        var radius = d.split('__')[0];
        var font = d.split('__')[1];
        var y = (nodes.length === 1) ? font/4 : ((2*i+1-nodes.length)*font)/2;
        // console.log(y);
        return y; 
      })
      .text(function(d) {
        return d.split('__')[2];
      });
});
// Returns a flattened hierarchy containing all leaf nodes under the root.
function classes(root) {
  var classes = [];
  function recurse(name, node) {
    if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
    else classes.push({
      id: node.ID,
      packageName: name,
      className: node.name,
      value: node.size
    });
  }
  recurse(null, root);
  return {children: classes};
}
d3.select(self.frameElement).style("height", diameter + "px");

var chart = $(".bubble"),
    aspect = chart.width() / chart.height(),
    container = chart.parent();
$(window).on("resize", function() {
  var targetWidth = container.width();
  chart.attr("width", targetWidth);
  chart.attr("height", Math.round(targetWidth / aspect));
}).trigger("resize");