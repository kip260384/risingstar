'use strict';

require('datatables.net');
require('datatables.net-bs');
require('./jquery-ui.min');
require('./jquery.tree-multiselect');

// ajax request for trips
function fetchEngineers(params) {
    console.log('fetch engineers with params');
    console.log(params);
    $.ajax({
        method: "GET",
        url: "/api/v1/engineers/filter",
        dataType: "JSON",
        data:{
            city: params.city,
            skill: params.skill
        }
    }).done(function (response) {
        // console.log(response);
        var rTable = $('#table-engineers').dataTable();
        rTable.fnClearTable();
        if (response.data.length > 0) {
            rTable.fnAddData(response.data);
        }
    }).fail(function (error) {
        console.log('error while fetching engineers');
        console.log(error);
    }).then(function () {
        $('.loadtext').remove();
        $('#table-engineers').find('tbody').removeClass('loading');
    });
}

$(document).ready(function() {
    console.log('engineers.js loaded...');

    // render select-multitree
    var params = {
        sortable: true,
        searchable: true,
        hideSidePanel: true
    };

    $('#skill_filter').treeMultiselect(params);

    // render table with engineers, do ajax request for engineers...
    var table = $('#table-engineers').DataTable({
        language: {
            'url': '/js/datatables-ru.json'
        },
        pageLength: 25,
        ajax: "/api/v1/engineers/filter",
        columns: [
            { data: 'FIO' },
            { data: 'City' },
            { data: 'Position' }
            // { data: 'EmployeeID' }
        ],
        columnDefs: [
            {
                targets: 0,
                render: function(data, type, full) {
                    if(window.location.pathname.indexOf('manager_engineers')>=0){
                        return '<a href="/rise/' + full.EmployeeID+'">' + data + '</a>';
                    }else{
                        return '<a href="/profile/' + full.EmployeeID + '/cabinet">' + data + '</a>';
                    }
                }
            }
            // {
            //     targets: 3,
            //     orderable: false,
            //     render: function (data, type, full) {
            //         if (userRole.hasOwnProperty('admin') || userRole.hasOwnProperty('manager')) {
            //         // if (userRole.indexOf('admin') > -1 || userRole.indexOf('manager') > -1) {
            //         //в этом варианте на всегда срабатывает функция indexOf т.к. userRole не строка
            //         //     Нужно проверить правильно ли ставятся кнопки ..
            //             return '<a target="blank" href="/rise/' + data + '"><span class="glyphicon glyphicon-export"></span></a>';
            //         }
            //         return '';
            //     }
            // }
        ],
        order: [
            [0, 'asc']
        ]
    });

    $('.engineers__filter__submit').on('click', function(event) {
        event.preventDefault();
        console.log('filter form submitted...');
        var params = [];
        params['city'] = $('#city').val();
        params['skill'] = $('#skill_filter').val();
        $('#table-engineers').find('tbody').addClass('loading');
        $('#table-engineers').append('<span class="loadtext" style="position: absolute; top: 50%; left: 50%; font-size: 20px;">Загрузка...</span>');
        fetchEngineers(params);
    });

    // $('#engineers_submit_btn').click();

});