'use strict';
/* Morris.js Charts */
// Stars chart
$(function() {

  var months = ['Янв.', 'Фев.', 'Мрт', 'Апр.', 'Май', 'Июн.', 'Июл.', 'Авг.', 'Сен.', 'Окт.', 'Нбр', 'Дек.'];

  var area = Morris.Line({
    element: 'revenue-chart',
    resize: true,
    data: [],
    xkey: 'day',
    ykeys: ['users', 'views'],
    labels: ['Посетители', 'Просмотры'],
    // xLables: 'day',
    // xLabelFormat: function(x) { // <--- x.getMonth() returns valid index
    //   // var month = months[x.getMonth()];
    //   // console.log(x);
    //   return x;
    // },
    // dateFormat: function(x) {
    //   console.log(x);
    //   // var month = months[new Date(x).getMonth()];
    //   // var year = new Date(x).getFullYear();
    //   console.log(new Date(x).toString());
    //   return x;
    // },
    lineColors: ['#a0d0e0', '#c2ecb0'],
    hideHover: 'auto'
  });

  function getChartData(year, chart){
    $.ajax({
      type: "GET",
      dataType: 'json',
      url: "/api/v1/visitchart", // This is the URL to the API
      data: { year: year }
    })
    .done(function( data ) {
      // When the response to the AJAX request comes back render the chart with new data
      // console.log(data);
      chart.setData(data);
    })
    .fail(function() {
      // If there is no communication between the server, show an error
      console.log('mymorris.js - there is no communication between the server');
    });
  }

  $(document).ready(function() {
    getChartData(2017, area);
  });

})