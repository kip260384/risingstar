require('./select2.full');

function checkPosition() {
    console.log('checkPosition...');
    var msg = '<span id="position-tooltip" class="glyphicon glyphicon-info-sign" data-toggle="tooltip" role="tooltip" title="Необходимо выбрать целевую должность!"><span>';
    console.log($('#WishedPosition').val());
    if ($('#WishedPosition').val() == 0) {
        console.log('append tooltip...');
        // $('#circle__wishedposition').append(msg);
        $('#circle__wishedposition').attr({
            "data-toggle": "tooltip",
            "role": "tooltip",
            "title": "Необходимо выбрать целевую должность!"
        });
    } else {
        // $('#position-tooltip').remove();
        $('#circle__wishedposition').removeAttr("data-toggle").removeAttr("role").removeAttr("title");
    }
}

$(document).ready(function() {
    $("#WishedCities").select2({
        placeholder: "    Выбрать города",
        theme: "default"
    });
    $("#WishedPosition").select2({
        placeholder: "Выбрать должность",
        theme: "default"
    });
    var $eventSelect = $("#WishedCities");
    $eventSelect.on("select2:select", function (e) {
        $.ajax({
            method: "GET",
            url: "/api/v1/add_wishedcity",
            dataType: "JSON",
            data:{
                city: e.params.data.text
            }
        }).done(function (response) {
            // console.log(response);
        }).fail(function (error) {
            console.log('The wished city hasn`t been added');
        });
    });

    $eventSelect.on("select2:unselect", function (e) {
        $.ajax({
            method: "GET",
            url: "/api/v1/del_wishedcity",
            dataType: "JSON",
            data:{
                city: e.params.data.text
            }
        }).done(function (response) {
            // console.log(response);
        }).fail(function (error) {
            console.log('The wished city hasn`t been deleted');
        });
    });

    checkPosition();
    $('#WishedPosition').change(function() {
       checkPosition();
       var position = this.value;
        $.ajax({
            method: "GET",
            url: "/api/v1/set_WishedPosition",
            dataType: "JSON",
            data:{
                position: this.value
            }
        }).done(function (response) {
            // console.log(response);
            if (position > 0 && $('#circle__wishedposition').hasClass('active')) {
                console.log('trigger click...');
                $('#circle__wishedposition').removeClass('active');
                document.getElementById('circle__wishedposition').click();
            }
        }).fail(function (error) {
            console.log('Error while setting the wishedposition');
        });
    });
});