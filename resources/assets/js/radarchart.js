import Chart from 'chart.js';

// require('datatables.net');
// require('datatables.net-bs');
require('bootstrap-sass');

function fetchDataRadarChart(position = '') {
  var id = $('#employee_id').val();
  console.log(id);
  $.ajax({
    method: "GET",
    url: "/api/v1/skills/radar/" + id,
    dataType: "JSON",
    data:{
      'position': position
    }
  }).done(function (response) {
    console.log(response);
    drawRadarChart('myChart', response);
    // drawTableSkills('table-skills', response);
  }).fail(function (error) {
    console.log('error when fetching engineers');
    console.log(error);
  }).then(function () {
  });
}

function drawRadarChart(element, data) {
  // console.log(data);

  var oldChart = $('#myChart').data('chart');

  if (oldChart !== undefined) oldChart.destroy();

  var chartContainer = $('#chart');
  console.log(chartContainer.width());

  $('#myChart').remove();
  chartContainer.append('<canvas id="myChart" width="' + chartContainer.width() + '" height="' + chartContainer.width()*2/3 + '"></canvas>');

  var ctx = document.getElementById(element);

  // var data = {
  //   labels: [".NET", "Java", "Администрирование Windows систем", "Наставник", "Наличие наставника", "Сопровождение СКС", "Ведение переговоров",
  //   "Публичные выступления", "Центральные системы", "Рабочая станция", "Периферия", "Готовность к релокации", "Стратегические проекты ДРО", 
  //   "Сопровождение ПТС", "Английский", "ITIL, ITSM", "Управление персоналом"],
  //   datasets: [
  //     {
  //       label: "Мои компететенции",
  //       backgroundColor: "rgba(179,181,198,0.2)",
  //       borderColor: "rgba(179,181,198,1)",
  //       pointBackgroundColor: "rgba(179,181,198,1)",
  //       pointBorderColor: "#fff",
  //       pointHoverBackgroundColor: "#fff",
  //       pointHoverBorderColor: "rgba(179,181,198,1)",
  //       data: [1, 2, 0, 3, 1, 0, 0, 2, 3, 1, 0, 0, 1, 2, 3, 2, 0, 1]
  //     },
  //     {
  //       label: "Компетенции должности",
  //       backgroundColor: "rgba(255,99,132,0.2)",
  //       borderColor: "rgba(255,99,132,1)",
  //       pointBackgroundColor: "rgba(255,99,132,1)",
  //       pointBorderColor: "#fff",
  //       pointHoverBackgroundColor: "#fff",
  //       pointHoverBorderColor: "rgba(255,99,132,1)",
  //       data: [2, 3, 1, 3, 2, 1, 2, 0, 0, 1, 2, 0, 1, 3, 1, 2, 3, 1]
  //     }
  //   ]
  // };
  var data = {
    data: data.data,
    labels: data.labels,
    datasets: [
      {
        label: "Мои компететенции",
        backgroundColor: "rgba(90,117,229,0.2)",
        borderColor: "rgba(90,117,229,1)",
        pointBackgroundColor: "rgba(90,117,229,1)",
        pointBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(90,117,229,1)",
        data: data.myskills
      },
      {
        label: "Компетенции должности",
        backgroundColor: "rgba(255,99,132,0.2)",
        borderColor: "rgba(255,99,132,1)",
        pointBackgroundColor: "rgba(255,99,132,1)",
        pointBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(255,99,132,1)",
        data: data.skillsbyposition
      }
    ]
  };

  var myRadarChart = new Chart(ctx, {
    type: 'radar',
    data: data,
    options: {
      legend: {
        position: 'bottom'
      },
      scale: {
        // beforeFit: function (scale) {
        //     if (scale.chart.config.data.labels.length === 3) {
        //         var pointLabelFontSize = Chart.helpers.getValueOrDefault(scale.options.pointLabels.fontSize, Chart.defaults.global.defaultFontSize);
        //         scale.height *= (2 / 1.5)
        //         scale.height -= pointLabelFontSize;
        //     }
        // },
        // afterFit: function (scale) {
        //     if (scale.chart.config.data.labels.length === 3) {
        //         var pointLabelFontSize = Chart.helpers.getValueOrDefault(scale.options.pointLabels.fontSize, Chart.defaults.global.defaultFontSize);
        //         scale.height += pointLabelFontSize;
        //         scale.height /= (2 / 1.5);
        //     }
        // },
        pointLabels: {
          // fontSize: 10,
          fontColor: '#5a75e5'
          // callback: function(value, index, values) {
          //   return value;
          // }
        },
        ticks: {
          // backdropPaddingX: 20,
          // autoSkipPadding: 0,
          fontSize: 16,
          beginAtZero: true,
          labelOffset: 100,
          min: 0,
          max: 3,
          stepSize: 1,
          callback: function(value, index, values) {
              return value;
          }
        }
      }
    }
  });

  $('#myChart').data('chart', myRadarChart);

  // console.log(myRadarChart);

  $('#myChart').click(function (e) {
        var helpers = Chart.helpers;
        // console.log(this);
        // console.log(myRadarChart.scale.ticks);
        var eventPosition = helpers.getRelativePosition(e, myRadarChart.chart);
        var mouseX = eventPosition.x;
        var mouseY = eventPosition.y;

        var activePoints = [];
        helpers.each(myRadarChart.scale.ticks, function (label, index) {
            // console.log(this);
            var n = this.pointLabels.length;
            for (var i = n - 1; i >= 0; i--) {
                //console.log(this);
                var pointLabelPosition = this.getPointPosition(i, this.getDistanceFromCenterForValue(this.options.reverse ? this.min : this.max) + 5);

                var pointLabelFontSize = helpers.getValueOrDefault(this.options.pointLabels.fontSize, Chart.defaults.global.defaultFontSize);
                var pointLabeFontStyle = helpers.getValueOrDefault(this.options.pointLabels.fontStyle, Chart.defaults.global.defaultFontStyle);
                var pointLabeFontFamily = helpers.getValueOrDefault(this.options.pointLabels.fontFamily, Chart.defaults.global.defaultFontFamily);
                var pointLabeFont = helpers.fontString(pointLabelFontSize, pointLabeFontStyle, pointLabeFontFamily);
                ctx.font = pointLabeFont;

                var labelsCount = this.pointLabels.length,
                    halfLabelsCount = this.pointLabels.length / 2,
                    quarterLabelsCount = halfLabelsCount / 2,
                    upperHalf = (i < quarterLabelsCount || i > labelsCount - quarterLabelsCount),
                    exactQuarter = (i === quarterLabelsCount || i === labelsCount - quarterLabelsCount);
                var width = this._pointLabelSizes[i].w;
                // var width = ctx.measureText(this.pointLabels[i]).width;
                var height = pointLabelFontSize;

                var x, y;

                // console.log(width);

                if (i === 0 || i === halfLabelsCount)
                    x = pointLabelPosition.x - width / 2;
                else if (i < halfLabelsCount)
                    x = pointLabelPosition.x;
                else
                    x = pointLabelPosition.x - width;

                if (exactQuarter)
                    y = pointLabelPosition.y - height / 2;
                else if (upperHalf)
                    y = pointLabelPosition.y - height;
                else
                    y = pointLabelPosition.y
                
                if ((mouseY >= y && mouseY <= y + height) && (mouseX >= x && mouseX <= x + width)) {
                    // console.log(this.pointLabels[i]);
                    activePoints.push({ index: i, label: this.pointLabels[i] });
                }
            }
        }, myRadarChart.scale);
        
        var firstPoint = activePoints[0];
        if (firstPoint !== undefined) {
            console.log(data);
            console.log(data.data[firstPoint.label]);
            console.log(data.data[firstPoint.label].ID);
            // var labelLinks = [];
            // data.labels.map(function(item, index) {
            //     labelLinks[item.text] = item.id;
            // });
            // console.log(labelLinks);
            // console.log(labelLinks[firstPoint.label]);
            console.log(firstPoint.index + ': ' + firstPoint.label);
            var win = window.open('/viewskill/' + data.data[firstPoint.label].ID, '_self');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }
        }
    });

}

// function drawTableSkills(element, data) {
//   console.log(data);
//   var table = $(element);
//   $.each(data.data, function(index, item) {
//     console.log(item);
//     var name = '<td><a href="/viewskill/' + item.ID + '">' + index + '</a></td>';
//     var SkillLevel = item.SkillLevel ? item.SkillLevel :  0;
//     var needSkill = item.NeedLevel ? item.NeedLevel : 0;
//     var className = ((SkillLevel - needSkill) < 0) ? 'danger' : '';
//     var mySkill = '<td>' + SkillLevel + '</td>';
//     var needSkill = '<td>' + needSkill + '</td>';
//     $('#' + element + ' tr:last').after('<tr class="' + className + '">' + name + mySkill + needSkill +'</tr>');
//   });
// }

$(document).ready(function() {
  fetchDataRadarChart();

  // $('#circle__wishedposition').on('shown.bs.tooltip', function() {
  //   console.log('show tooltip...');
  // });
  // $('#circle__wishedposition').on('mouseenter', function() {
  //   console.log('hide tooltip...');
  // });

  $('.circle__position').on('click', function(event) {
    event.preventDefault();
    console.log('.circle__position click...')
    var thisLink = $(this);
    if (thisLink.hasClass('active')) return;
    if (thisLink.data('position')) {
      var wishedPosition = $('#WishedPosition').val();
      if (wishedPosition > 0) {
        fetchDataRadarChart(wishedPosition);
        $('.circle__position').removeClass('active');
        thisLink.addClass('active');
      }
    } else {
      fetchDataRadarChart();
      $('.circle__position').removeClass('active');
      thisLink.addClass('active');
    }
  });

});

