require('./jquery.tree-multiselect');

$( document ).ready(function() {
    var params = {
        sortable: true,
        searchable: true,
        hideSidePanel: true
    };

    $("#skills_tree").treeMultiselect(params);
});
