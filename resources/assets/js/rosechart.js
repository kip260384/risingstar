$(function(){
    var radarOptions = {
        palette: "soft",
        dataSource: dataSource[0].values,
        title: "Компетенции",
        commonSeriesSettings: {
            type: "stackedbar"
        },
        margin: {
            bottom: 50,
            left: 100
        },
        onLegendClick: function(e){
            var series = e.target;
            if (series.isVisible()) {
                series.hide();
            } else {
                series.show();
            }
        },
        argumentAxis: {
            discreteAxisDivisionMode: "crossLabels",
            firstPointOnStartAngle: true
        }, 
        valueAxis: {
            valueMarginsEnabled: false
        },
        "export": {
            enabled: true
        },
        series: [{ valueField: "val1", name: "1.3-4 m/s" },
                { valueField: "val2", name: "4-8 m/s" }
        ]
    };
    
    var radar = $("#radarChart").dxPolarChart(radarOptions).dxPolarChart("instance");
    
    $("#radarPeriods").dxSelectBox({
        width: 300,
        dataSource: dataSource,
        displayExpr: "period",
        valueExpr: "values",
        value: dataSource[0].values,
        onValueChanged: function(e){
            radar.option("dataSource", e.value);
        }
    });
});