require('./jquery.tree-multiselect');

$( document ).ready(function() {
    var params = {
        hideSidePanel: true
    };
    $("select#skill_catalog_tree").treeMultiselect(params);

    params = {
        hideSidePanel: true,
        allowBatchSelection: false,
        searchable: true,
        startCollapsed: false
    };
    $("select#skills_tree").treeMultiselect(params);
});
