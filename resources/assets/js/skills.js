'use strict';

require('./jquery.tree-multiselect');

require('datatables.net');
require('datatables.net-bs');
require('./jquery-ui.min');

$( document ).ready(function() {
    var params = {
        // sortable: true,
        hideSidePanel: true
    };

    $("select#skill_catalog_tree").treeMultiselect(params);

    // render table with engineers, do ajax request for engineers...
    var table = $('#skills_table').DataTable({
        language: {
            'url': '/js/datatables-ru.json'
        },
        pageLength: 25,
        ajax: "/api/v1/get_skills_by_cat",
        columns: [
            { data: 'name' },
            { data: 'manager_fio' },
            { data: 'expert_fio' },
            { data: 'desc' },
            { data: 'id' }
        ],
        columnDefs: [
            {
                targets: 0,
                orderable: true,
                render: function(data, type, full) {
                    return '<a href="/viewskill/' + full.id + '">' + data + '</a>';
                }
            },
            {
                targets: 3,
                render: function (data, type, full) {
                    return data.substring(0, 250);
                }
            },
            {
                targets: 4,
                orderable: false,
                render: function(data, type, full) {
                    let editBtn = "<a href='/editskill/" + data + "'><span class='glyphicon glyphicon-pencil'></span></a>";
                    let deleteBtn = "<a href='/delskill/" + data + "'><span class='glyphicon glyphicon-remove' style='color:red'></span></a>";

                    if (!(typeof userRole['admin'] === 'undefined')) {
                        var str = "<td>" + editBtn + deleteBtn + "</td>";
                    } else if ((userRole.manager !== undefined) && (userRole.manager.indexOf(data) > -1)) {
                        var str = "<td>" + editBtn + "</td>";
                    } else {
                        var str = "";
                    }

                    return str;
                }
            }
        ],
        order: [
            [0, 'asc']
        ]
    });

    $("#skill_catalog_tree").change(function () {
        // $('#skills_table').find("tr:not(:nth-child(1))").remove();
        var skills_arr = $("#skill_catalog_tree").val();

        $('#skills_table').find('tbody').addClass('loading');
        $('#skills_table').append('<span class="loadtext" style="position: absolute; top: 50%; left: 50%; font-size: 20px;">Загрузка...</span>');

        $.ajax({
            url: '/api/v1/get_skills_by_cat',
            dataType: 'json',
            data: {
                'skills_arr': skills_arr
            }
        }).done(function (response){
            console.log('success get_skills_by_cat ajax call');
            console.log(response);
            var rTable = $('#skills_table').dataTable();
            rTable.fnClearTable();
            if (response.data.length > 0) {
                rTable.fnAddData(response.data);
            }
            // $.each(response, function (i, val){
            //     let editBtn = "<a href='/editskill/" + val.id + "'><span class='glyphicon glyphicon-pencil'></span></a>";
            //     let deleteBtn = "<a href='/delskill/" + val.id + "'><span class='glyphicon glyphicon-remove' style='color:red'></span></a>";

            //     if (!(typeof userRole['admin'] === 'undefined')) {
            //         str = "<td>" + editBtn + deleteBtn + "</td>";
            //     } else if ((userRole.manager !== undefined) && (userRole.manager.indexOf(val.id) > -1)) {
            //         str = "<td>" + editBtn + "</td>";
            //     } else {
            //         str = "";
            //     }

            //     $("#skills_table > tbody").append(
            //         "<tr><td><a href='/viewskill/"+val.id+"'>"+val.name+"</a></td><td>"+val.manager_fio+"</td><td>"+val.expert_fio+"</td><td>"+val.desc.substring(0, 250)+"</td>"+str+"</tr>"
            //     );
            // });

        }).fail(function(response){
            console.log('error get_skills_by_cat ajax call');
            console.log(err);
        }).then(function () {
            $('.loadtext').remove();
            $('#skills_table').find('tbody').removeClass('loading');
        });;
    });

    // $("#skill_catalog_tree").change();
});
