@extends('layouts.main')

  @section('content')

    <section class="content-header">
      <h1>
        Добавить/редактировать компетенцию
      </h1>
    </section>

    <main class="content">
      @if(count($errors)>0)
        <div class="alert alert-danger">
          <ul>
            @foreach($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
          </ul>
        </div>
      @endif

      <div class="row">
        <div class="col-md-12">
          <form class="form" action="/addskill_post" method="POST">
            {{ csrf_field() }}
            @if(isset($id))
              <input type="hidden" value="{{$id}}" name="id">
            @endif

            <div class="row col-md-12">
              <div class="form-group">
                <label for="name">Название</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="{{ old('name', $name) }}" required>
              </div>
            </div>

            <div class="row col-md-12">
              <div class="form-group">
                <label for="group">Функция/Категория</label>
                <select class="form-control" id="skillgroup" name="skillgroup">
                  @foreach ($catalog_flat as $row)
                    @if($row['Value'] == old('skillgroup', $skillgroup))
                      <?php $str = "selected='selected'"; ?>
                    @else
                      <?php $str = ""; ?>
                    @endif
                    <option value="{{$row['Value']}}" {{$str}}>{{$row['DisplayText']}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col-md-12 row">
              <div class="form-group">
                <label for="position">Должность</label>
                <input type="text" class="form-control" name="position" value="{{{ old('position', $position) }}}">
              </div>
            </div>

            <div class="row col-md-12">
              <div class="form-group">
                <label for="desc">Описание</label>
                <textarea id="desc" name="desc" class="form-control summernote" rows="3">{{old('desc', $desc)}}</textarea>
              </div>
            </div>

            <div class="row col-md-12">
              <div class="form-group">
                <label for="desc1">Описание критерия 1 уровня</label>
                <textarea id="desc1" name="desc1" class="form-control summernote" rows="3">{{old('desc1', $desc1)}}</textarea>
              </div>
            </div>

            <div class="row col-md-12">
              <div class="form-group">
                <label for="desc2">Описание критерия 2 уровня</label>
                <textarea id="desc2" name="desc2" class="form-control summernote" rows="3">{{old('desc2', $desc2)}}</textarea>
              </div>
            </div>

            <div class="row col-md-12">
              <div class="form-group">
                <label for="desc3">Описание критерия 3 уровня</label>
                <textarea id="desc3" name="desc3" class="form-control summernote" rows="3">{{old('desc3', $desc3)}}</textarea>
              </div>
            </div>

            <div class="row col-md-12">
              <div class="form-group">
                <label for="materials">Методические материалы для изучения</label>
                <textarea id="materials" name="materials" class="form-control summernote" rows="3">{{old('materials', $materials)}}</textarea>
              </div>
            </div>

            <div class="row col-md-12">
              <div class="form-group">
                <label for="test1">Тесты на 1 звезду</label>
                <textarea id="test1" name="test1" class="form-control summernote" rows="3">{{old('test1', $test1)}}</textarea>
              </div>
            </div>

            <div class="row col-md-12">
              <div class="form-group">
                <label for="test2">Тесты на 2 звезды</label>
                <textarea id="test2" name="test2" class="form-control summernote" rows="3">{{old('test2', $test2)}}</textarea>
              </div>
            </div>

            <div class="row col-md-12">
              <div class="form-group">
                <label for="test3">Тесты на 3 звезды</label>
                <textarea id="test3" name="test3" class="form-control summernote" rows="3">{{old('test3', $test3)}}</textarea>
              </div>
            </div>

            <div class="col-md-12 row">
              <div class="form-group">
                <label for="manager">Менеджер</label>
                <!-- <input id="manager" name="manager" class="form-control"></input> -->
                <input name="manager_id" id="manager_id" type="hidden" value="{{ old('manager_id', $manager_id) }}" required>
                <input type="text" class="form-control" name="manager_fio" id="manager_fio" value="{{ old('manager_fio', $manager_fio) }}" placeholder="Введите ФИО" required>
                <img class="spinner" src="/img/spin.gif" style="display: none; position: absolute; right: 15px; width: 36px; height: 36px; top: 28px;">
              </div>
            </div>

            <div class="row col-md-12">
              <div class="form-group">
                <label for="expert">Эксперт</label>
                <input name="expert_id" id="expert_id" type="hidden" value="{{ old('expert_id', $expert_id) }}" required>
                <input type="text" class="form-control" name="expert_fio" id="expert_fio" value="{{ old('expert_fio', $expert_fio) }}" placeholder="Введите ФИО" required>
                <img class="spinner" src="/img/spin.gif" style="display: none; position: absolute; right: 15px; width: 36px; height: 36px; top: 28px;">
              </div>
            </div>

            @if(isset($skill) && $skill->Status == 5)
              <div class="row">
              <div class="panel panel-default">
                <div class="panel-heading text-center"><h2>Доработка</h2></div>
                <div class="panel-body">
                  <div class="form-group">
                    <label for="comment">Комментарий доработки</label>
                    <div class="jumbotron" id="comment">
                      @foreach($comments as $comment)
                        {{$comment->Date}} - {{$comment->FIO}}<br>
                        {{$comment->Description}}
                        <br>
                      @endforeach
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="your_comment">Ваш комментарий</label>
                    <textarea id="your_comment" name="your_comment" class="form-control" rows="3"></textarea>
                  </div>
                </div>
              </div>
              </div>
            @endif


            <button type="submit" name="send" class="btn btn-success">На рассмотрение</button>
            <button type="submit" name="save" class="btn btn-success">Сохранить как черновик</button>
          </form>
        </div>
      </div>

    </main>

  @stop

  @section('scripts')

    <link rel="stylesheet" type="text/css" href="/vendor/summernote/css/summernote.css">
    <script type="text/javascript" src="/vendor/summernote/js/summernote.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        $('.summernote').summernote({
          minHeight: 150
        });
      })
    </script>

    <script type="text/javascript" src="{{ mix('js/addskills.js') }}"></script>

  @stop