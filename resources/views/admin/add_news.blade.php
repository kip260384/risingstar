@extends('layouts.main')

@section('content')

    <section class="content-header">
        <h1>
            Создать новость
        </h1>
    </section>

    <main class="content">
        <div class="row">
            <div class="col-md-6">
                <form class="form" action="/add_news_post" method="POST">
                    {{ csrf_field() }}

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Type">Тип</label>
                            <select class="form-control" id="Type" name="Type">
                                @foreach ($types as $row)
                                    @if($row['Value'] == 'wtf')
                                        <?php $str = "selected='selected'"; ?>
                                    @else
                                        <?php $str = ""; ?>
                                    @endif
                                    <option value="{{$row['Value']}}" {{$str}}>{{$row['DisplayText']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    {{--<div class="col-md-12">--}}
                        {{--<div class="form-group">--}}
                            {{--<label for="Header">Header</label>--}}
                            {{--<input type="text" class="form-control" id="Header" name="Header" placeholder="Header" value="" required>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Body">Body</label>
                            <textarea id="Body" name="Body" class="form-control summernote" rows="3"></textarea>
                        </div>
                    </div>
                    <button type="submit" name="send" class="btn btn-success">Создать</button>
                </form>
            </div>
        </div>
    </main>

@stop

@section('scripts')

    <link rel="stylesheet" type="text/css" href="/vendor/summernote/css/summernote.css">
    <script type="text/javascript" src="/vendor/summernote/js/summernote.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.summernote').summernote({
                minHeight: 150
            });
        });
    </script>

@stop