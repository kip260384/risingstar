@extends('layouts.main')

@section('content')

    <section class="content-header">
        <h1>
            Список должностей РЦС
        </h1>
    </section>

    <main class="content">
        <div class="row">
            <div class="col-md-8">
                <table name="Positions" class="table">
                    <tr><th>ID</th><th>Должность</th></tr>
                    @foreach ($Positions as $Position)
                        <tr>
                            <td>{{$Position->ID}}</td>
                            <td><a href="/adm_positionprofile/{{$Position->ID}}">{{$Position->Position}}</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>

        </div>
    </main>

@stop

@section('scripts')



@stop