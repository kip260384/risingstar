@extends('layouts.main')

@section('content')

    <section class="content-header">
        <h1>
            Управление уровнями компетенций {{$Position or ''}}
        </h1>
    </section>

    <main class="content">
        <div class="row">
            <div class="col-md-8">
                <form class="form" action="/save_position_skills" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="position_id" value="{{$position_id}}">
                    <table class="table">
                        <tr><th>Компетенция</th><th>Уровень</th></tr>
                        @foreach($skills as $skill)
                            <tr>
                                <td>{{$skill->SkillName}}</td>
                                <td>
                                    <label class="radio-inline"><input type="radio" value="0" name="Skills[{{$skill->SkillID}}]" @if($skill->SkillLevel == 0) checked="checked" @endif>0</label>
                                    <label class="radio-inline"><input type="radio" value="1" name="Skills[{{$skill->SkillID}}]" @if($skill->SkillLevel == 1) checked="checked" @endif>1</label>
                                    <label class="radio-inline"><input type="radio" value="2" name="Skills[{{$skill->SkillID}}]" @if($skill->SkillLevel == 2) checked="checked" @endif>2</label>
                                    <label class="radio-inline"><input type="radio" value="3" name="Skills[{{$skill->SkillID}}]" @if($skill->SkillLevel == 3) checked="checked" @endif>3</label>
                                </td>
                                <td>
                                    <a href="/remove_position_skill/{{$position_id}}/{{$skill->SkillID}}"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <button type="submit" class="btn btn-success">Сохранить</button>
                </form>

                <hr>
<h2>Добавить компетенцию в профиль дожности</h2>
                @if (isset($catalogStr))
                    <form class="form" action="/add_position_skills" method="POST">
                        {!! csrf_field() !!}
                        <input type="hidden" name="position_id" value="{{$position_id}}">
                        {!! $catalogStr !!}
                        <button type="submit" class="btn btn-success pull-right">Добавить</button>
                    </form>
                @endif

            </div>
        </div>
    </main>

@stop

@section('scripts')

    <script type="text/javascript" src="{{ mix('js/adm_positionprofile.js') }}"></script>

@stop