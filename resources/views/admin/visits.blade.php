@extends('layouts.main')

@section('content')

    <section class="content-header">
        <h1>Статистика посещений сайта</h1>
    </section>

    <main class="content">
        <div class="">
            
                <div class="chart tab-pane active" id="revenue-chart"></div>
            <table class="table table-condensed table-hover1" style="border-collapse:collapse; margin-bottom: 0px;">
                <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Кол-во уникальных посетителей</th>
                        <th>Кол-во просмотров</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($visits as $key => $visit)
                        <tr data-toggle="collapse" data-target="#{{ str_replace('.', '', $key) }}" class="accordion-toggle" style="cursor: pointer;">
                            <td>{{ $key }}</td>
                            <td>{{ $visit->count() }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="row-hidden" style="padding: 0">
                                <div id="{{ str_replace('.', '', $key) }}" class="collapse" style="padding-left: 15px;">
                                    <table class="table table-condensed" style="border-collapse:collapse; background: none; margin-bottom: 0px;">
                                        <!-- <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead> -->
                                        <tbody>
                                            @foreach ($visit as $key2 => $person)
                                                <tr data-toggle="collapse" data-target="#{{{ str_replace('.', '', $key) . str_replace('$', '', explode('\\', $key2)[1]) }}}" class="accordion-toggle info" style="cursor: pointer;">
                                                    <td>{{ $person[0]->FIO or $key2 }}</td>
                                                    <td>{{ $person[0]->BusinessLine or '-' }}</td>
                                                    <td>{{ $person[0]->City or '-' }}</td>
                                                    <td>{{ $person->count() }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="row-hidden" style="padding: 0">
                                                        <div id="{{{ str_replace('.', '', $key) . str_replace('$', '', explode('\\', $key2)[1]) }}}" class="collapse" style="padding-left: 15px;">
                                                            <table class="table table-condensed" style="border-collapse:collapse; background: none; margin-bottom: 0px;">
                                                                <tbody>
                                                                    @foreach ($person as $detail)
                                                                        <tr class="success">
                                                                            <td><a href="{{ $detail->Url or 'url error' }}" target="_blank">{{ $detail->Url or 'url error' }}</a></td>
                                                                            <td>{{ $detail->createdat or 'date error' }}</td>
                                                                            <td></td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </main>

@stop

@section('scripts')

  <script type="text/javascript" src="{{ mix('js/mymorris.js') }}"></script>

@stop