<div class="row">

    @include('dashboard.stats')

</div>

<div style="margin-bottom: 15px;">

    @include('dashboard.slider')

</div>

<div class="row">
  {{--The left column--}}
    <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-12" style="margin-bottom: 15px;">
                <section class="">
                    <!-- table skills -->
                    {{--@include('dashboard.skills-table')--}}
                </section>
                <section class="connectedSortable ui-sortable">
                    <img src="{{asset('/files/comics.png')}}" class="" data-toggle="modal" data-target="#myModal" width='100%' style="cursor: pointer">
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12" style="margin-bottom: 15px;">
              <!-- bubble skills -->
              @include('dashboard.skills-bubble')
              </section>
            </div>
        </div>
    </div>

    {{--The right column--}}
    <div class="col-lg-6">
        <div class="row">
            {{--<div class="col-lg-12">--}}
                {{--@include('dashboard.stars-per-month')--}}
            {{--</div>--}}
            <div class="col-lg-12" style="margin-bottom: 15px;">
                <video width="100%" poster="/img/poster.jpg" controls>
                    <source src="/files/Trailer_RS_480.mp4">
                </video>
            </div>
        </div>
        {{--<div class="row">--}}
            {{--<div class="col-lg-12" style="margin-bottom: 15px;">--}}
                {{--<img src="/img/newsband.jpg" width="100%">--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="row">
            <div class="col-lg-12" style="margin-bottom: 15px;">
                <a href="/files/prezentation.pdf"><img src="/img/presentation.jpg" width="100%"></a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                @include('dashboard.news_feed')
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Восходящая звезда</h4>
            </div>
            <div class="modal-body">
                <img src="{{asset('/files/comics.png')}}" width='100%'>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>

    </div>
</div>