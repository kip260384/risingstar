<div class="box box-primary">
  <!-- <div class="box-header with-border">
    <h3 class="box-title">Фильтр</h3>
  </div> -->
  <!-- /.box-header -->
    <div class="box-body">
      <!-- form start -->
      <form role="form" method="GET" action="/api/v1/dashboard/filter">
        <div class="form-group col-xs-12 col-md-4">
          <label for="department">Город</label>
          <select name="department" class="form-control">
            <option value="">Все</option>
            @foreach ($cities as $city)
              <option value="{{ $city->city }}">{{ $city->city }}</option>
            @endforeach
          </select>

        </div>
        <div class="form-group col-xs-12 col-md-4">
          <label for="exampleInputEmail1" class="hidden-xs hidden-sm">&nbsp;</label>
          <button type="submit" class="form-control btn btn-primary">Применить</button>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
</div>