@extends('layouts.main')

@section('content')

  <section class="content-header">
    <!-- <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol> -->
  </section>

  <main class="content">

    @include('dashboard.dashboard')

  </main>

@stop

@section('scripts')

  <script type="text/javascript" src="{{ mix('js/dashboard.js') }}"></script>

@stop