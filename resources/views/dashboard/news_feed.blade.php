<div class="col-lg-12">
    <span class="lya_big_ul_blue">Лента новостей</span>
</div>
@foreach($news as $line)
    <div class="col-xs-12">
        <div class="news_left">
            <span class="news_db news_db_{{$line->Type}}">{{ $line->Type }}</span>
            @php $authorized = LdapAuth::authorize('del_news'); @endphp
                @if($authorized)
                    <br>
                    <a href="/del_news/{{$line->ID}}" title="Удалить новость"><span class="glyphicon glyphicon-remove" style="color:red" aria-hidden="true"></span></a>
                @endif

        </div>
        <div class="news_right">
            {!! $line->Body !!}
        </div>
        <div class="pull-right">
            <strong>{{ Carbon\Carbon::parse($line->created_at)->format('d.m.Y') }}</strong>
        </div>
        <div style="clear: both"></div>
        <div class="lya_h_line"></div>
    </div>
@endforeach
