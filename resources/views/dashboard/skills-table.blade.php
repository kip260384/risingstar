<!-- Custom tabs (Charts with tabs)-->
<div class="nav-tabs-custom">
  <!-- Tabs within a box -->
  <ul class="nav nav-tabs pull-right ui-sortable-handle">
    <!-- <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li> -->
    <!-- <li><a href="#sales-chart" data-toggle="tab">Donut</a></li> -->
    <li class="pull-left header">&nbsp;&nbsp;Топ компетенций</li>
  </ul>
  <div class="underline"></div>
  <div class="tab-content no-padding">
    <!-- Morris chart - Sales -->
    <table class="table table-responsive table-hovered">
      <thead>
        <th>#</th>
        <th>Скилл</th>
        <th>Кол-во</th>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>ATM</td>
          <td>93</td>
        </tr>
        <tr>
          <td>2</td>
          <td>Windows Servers</td>
          <td>88</td>
        </tr>
        <tr>
          <td>3</td>
          <td>SM</td>
          <td>79</td>
        </tr>
        <tr>
          <td>4</td>
          <td>English</td>
          <td>65</td>
        </tr>
        <tr>
          <td>5</td>
          <td>Ремонт оргтехники</td>
          <td>60</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
