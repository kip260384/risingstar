<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="rs-info-box">
    <div class="rs-info-wrap">
      <div class="rs-info-box-left">
        Активные<br>сотрудники
      </div>
      <div class="v-lined pull-right"></div>
    </div>
    <div class="rs-info-box-right">
      {!! number_format($pr_appraised, 2, '.', '') !!}<small>%</small>
    </div>
  </div>
</div>

<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="rs-info-box">
    <div class="rs-info-wrap">
      <div class="rs-info-box-left">
        Сотрудники
      </div>
      <div class="v-lined pull-right"></div>
    </div>
    <div class="rs-info-box-right">
      {{$rcs_number}}
    </div>
  </div>
</div>

<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="rs-info-box">
    <div class="rs-info-wrap">
      <div class="rs-info-box-left">
        Эксперты
      </div>
      <div class="v-lined pull-right"></div>
    </div>
    <div class="rs-info-box-right">
      {{$ex_number}}
    </div>
  </div>
</div>

<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="rs-info-box">
    <div class="rs-info-wrap">
      <div class="rs-info-box-left">
        Звезды
      </div>
      <div class="v-lined pull-right"></div>
    </div>
    <div class="rs-info-box-right">
      {{$stars_count}}
    </div>
  </div>
</div>
