@if (isset($engineers))

    <table id="table-engineers" class="table table-responsive table-bordered table-hover" style="background: #fff;">
    <thead>
        <th>ФИО</th>
        <th>Город</th>
        <th>Должность</th>
    </thead>
    <tbody>
        <!-- table generates by datatable.js plugin in engineers.js -->
    </tbody>
    </table>

@endif