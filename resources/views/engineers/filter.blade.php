<form action="/api/v1/engineers/filter" method="GET">
  <!-- secelt cities -->
  <div class="form-group">
    <label for="city">Город</label>
    <select name="city" id="city" class="form-control">
      <option value="">Все</option>
      @foreach ($cities as $city)
        <option value="{{ $city->city }}">{{ $city->city }}</option>
      @endforeach
    </select>
    <input type="submit" class="btn btn-primary btn-block engineers__filter__submit" value="Применить" />
  </div>

  <div class="form-group">
    @if (isset($skill_filter))
      {!! $skill_filter !!}
    @endif
  </div>
  @if(isset($skill_filter))
    <input type="submit" id="engineers_submit_btn" class="btn btn-primary btn-block engineers__filter__submit" value="Применить" />
  @endif
</form>