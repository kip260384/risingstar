@extends('layouts.main')

@section('content')

  <section class="content-header">
    <!-- <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol> -->
  </section>

  <main class="content">

    <div class="row">

      <div class="col-md-4">

        @include('engineers.filter')

      </div>

      <div class="col-md-8">

        @include('engineers.engineers')

      </div>

    </div>


  </main>

@stop

@section('scripts')

  <script type="text/javascript" src="{{ mix('js/engineers.js') }}"></script>

@stop
