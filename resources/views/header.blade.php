<header class="main-header">

  <!-- Logo -->
  <a href="/" class="logo hidden-xs">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>В</b>З</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><img src="/img/logo.png" class="img-responsive"></span>
  </a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="visible-xs sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="profile_head_item pull-right">
      <div>
        <a href="/profile/{{{ isset(LdapAuth::user()->employeeid) ? LdapAuth::user()->employeeid : 'null' }}}/cabinet">
          <img width='50px' height='50px' class="circle" src="data:image/png;base64, {!! isset(LdapAuth::user()->thumbnailphoto) ? base64_encode(LdapAuth::user()->thumbnailphoto) : '' !!}">
          <span class="hidden-xs">{{{ isset(LdapAuth::user()->uname) ? LdapAuth::user()->uname : '' }}}</span>
        </a>
      </div>
    </div>
    {{--<div class="navbar-custom-menu">--}}
      {{--<ul class="nav navbar-nav">--}}
        {{--<!-- User Account Menu -->--}}
        {{--<li class="dropdown user user-menu">--}}
          {{--<!-- Menu Toggle Button -->--}}
          {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
            {{--<!-- The user image in the navbar-->--}}
            {{--<!-- <img src="/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->--}}
            {{--<img width='160px' height='160px' class="user-image" src="data:image/png;base64, {!! isset(LdapAuth::user()->thumbnailphoto) ? base64_encode(LdapAuth::user()->thumbnailphoto) : '' !!}" style='float:left'>--}}
            {{--<!-- hidden-xs hides the username on small devices so only the image appears. -->--}}
            {{--<span class="hidden-xs">{{{ isset(LdapAuth::user()->uname) ? LdapAuth::user()->uname : '' }}}</span>--}}
          {{--</a>--}}
          {{--<ul class="dropdown-menu">--}}
            {{--<li><a href="/profile">Профиль</a></li>--}}
            {{--<li><a href="/skillstorework">Компетенции на доработке</a></li>--}}
            {{--<li><a href="/skillsdraft">Черновики компетенций</a></li>--}}

  </nav>
</header>