<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-COMPATIBLE" content="IE=Edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ $title or 'Rising Star' }}</title>
  <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
  <link rel="shortcut icon" type="image/png" href="/favicon.png">
  <link rel="stylesheet" type="text/css" href="/plugins/morris/morris.css">
  <!-- <link rel="stylesheet" type="text/css" href="css/flexboxgrid.css"> -->
</head>
<body class="skin-blue sidebar-mini">
<div class="wrapper">
  @include('header')

  @include('sidebar')

  <div class="content-wrapper">
    @if (session('error'))
      <div class="row">
        <div class="alert alert-warning text-center">
          {{ session('error') }}
        </div>
      </div>
    @endif
    @if (session('status'))
      <div class="row">
        <div class="alert alert-success text-center">
          {{ session('status') }}
        </div>
      </div>
    @endif

    @yield('content')
  </div>

</div>



<script>
  window.Laravel = <?php echo json_encode([
          'csrfToken' => csrf_token(),
  ]); ?>
</script>
<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>

<script src="/js/raphael.2.1.0.min.js"></script>
<script type="text/javascript" src="/plugins/morris/morris.js"></script>
<script type="text/javascript">
  var userRole = {!! session()->has('roles') ? json_encode(session('roles')) : 'undefined'; !!};
  userRole = userRole ? userRole : [];
//  console.log(userRole);
</script>
<script>

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  function appendAvatar(element, user) {
    var userCreds = user ? (Array.isArray(user) ? user : user.split('\\')) : userAD;
    var srcAvatar = 'http://alfa/my/User%20Photos/Profile%20Pictures/' + userCreds[1] + '_LThumb.jpg';
    var newAvatar = $('<img src="' + srcAvatar + '">');

    newAvatar
      .on('load', function(event) {
        console.log('1 avatar loaded...');
        $(element).append(newAvatar.addClass('img-responsive nav-tabs-custom').css('margin', 'auto'));
      })
      .on('error', function(event) {
        console.log('1 load failed...');
        srcAvatar = 'http://alfa/my/User%20Photos/Profile%20Pictures/' + userCreds[0] + '_' + userCreds[1] + '_FSize.jpg';
        newAvatar = $('<img src="' + srcAvatar + '">');

        newAvatar
          .on('load', function(event) {
            console.log('2 avatar loaded...');
            $(element).append(newAvatar.addClass('img-responsive nav-tabs-custom').css('margin', 'auto'));
          })
          .on('error', function(event) {
            console.log('2 avatar load failed...');
            $(element).append('<img src="http://alfa/_layouts/15/images/person.gif" class="img-responsive nav-tabs-custom" style="margin: auto">');
          })
      });
  }

  $.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {

      /*delete empty fields and selected default value "-"*/

      if (this.value != '' && this.value != "—") {
        if (o[this.name] !== undefined) {
          if (!o[this.name].push) {
            o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value);
        } else {
          o[this.name] = this.value;
        }
      }
    });
    return o;
  };
</script>

@yield('scripts')

</body>
</html>