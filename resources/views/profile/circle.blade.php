@extends('profile.index')

@section('profile_content')
    <div class="row">
        <div class="col-md-12">
            <input type="hidden" name="employee_id" id="employee_id" value="{{ $employee_id or '' }}">
            <h4 style="color: #6186e1">Моя цель:</h4>
            <button class="btn pull-right sndlvl disabled" style="color: #fff">Добавить компетенцию</button>
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-6">
                    <select id="WishedCities" class="form-control" multiple {{{ isset($owner) && (!$owner) ? 'disabled' : ''}}}>
                        @foreach($cities as $city)
                            <?php
                                $selected = '';
                                foreach ($WishedCities as $wcity){
                                    if($wcity->City_name == $city->city){
                                        $selected = "selected='selected'";
                                        break;
                                    }
                                }
                            ?>
                            <option value="{{$city->city}}" {{$selected}}>{{$city->city}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <select id="WishedPosition" class="form-control">
                        <option value="0">Выбрать должность</option>
                        @foreach($Positions as $Position)
                            @php
                                $selected = '';
                                if($WishedPosition->ID == $Position->ID){
                                    $selected = "selected='selected'";
                                }
                            @endphp
                            <option value="{{$Position->ID}}" {{$selected}}>{{$Position->Position}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div id="chart" class="row">
                <div class="col-md-12 text-center" style="color: #6186e1; font-size: 18px; margin-bottom: 15px;">
                    <a href="#" class="circle__position active">текущая должность</a> | 
                    <a id="circle__wishedposition" data-position="wishedposition" href="#" class="circle__position">целевая должность</a>
                </div>
            </div>
            <!-- <div class="row">
                <table id="table-skills" class="table table-responsive table-bordered table-hover" style="background: #fff;">
                    <thead>
                    <th>Компетенция</th>
                    <th>Мой уровень</th>
                    <th>Необходимый уровень</th>
                    </thead>
                    <tbody> -->
                        <!-- table generates by datatable.js plugin in engineers.js -->
                    <!-- </tbody>
                </table>
            </div> -->

        </div>
    </div>
@stop


@section('scripts')

  <script type="text/javascript" src="{{ mix('js/radarchart.js') }}"></script>
  <script type="text/javascript" src="{{ mix('js/myprofile.js') }}"></script>

@stop
