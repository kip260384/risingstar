@extends('layouts.main')

@section('content')

    <section class="content-header">
        <div class="row">

            <div style="height: 30px"></div>
            <div class="col-xs-12">
                @if(isset($owner) && $owner)
                    <div class="h-menu-wrapper text-center">
                        <a href="/profile/{{{ isset(LdapAuth::user()->employeeid) ? LdapAuth::user()->employeeid : 'null' }}}/cabinet" class="{{{ in_array('cabinet', Request::segments()) ? 'active' : '' }}}">Личный кабинет</a>
                        <a href="/profile/{{{ isset(LdapAuth::user()->employeeid) ? LdapAuth::user()->employeeid : 'null' }}}/circle" class="{{{ in_array('circle', Request::segments()) ? 'active' : '' }}}">Колесо компетенций</a>
                        <a href="/profile/{{{ isset(LdapAuth::user()->employeeid) ? LdapAuth::user()->employeeid : 'null' }}}/rate" class="{{{ in_array('rate', Request::segments()) ? 'active' : '' }}}">Рейтинг</a>
                    </div>
                @else

                @endif
            </div>

        </div>
    </section>


    <main class="content">
        <div class="profile_wrapper">
            @yield('profile_content')
        </div>
    </main>

@stop

@section('scripts')


@stop