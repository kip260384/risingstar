@extends('profile.index')

@section('profile_content')
    <div class="row">
        <div id="profile__avatar" class="col-md-3"></div>
        <div class="col-md-9">
            <p class="lya_big_text">{{{ isset($user->FIO) ? $user->FIO : 'no FIO' }}}<u class="pull-right" style="font-size: 14px;">#44 место в рейтинге</u></p>
            <p class="">{{{ isset($user->Position) ? $user->Position : 'no position' }}}, {{{ isset($user->City) ? $user->City : 'Город не определен' }}}</p>
            <img src="/img/lk-feed.png" class="img-responsive">
        </div>
    </div>
    <div class="row v_line_1stlvl"></div>
    <div class="star-table">
        <div class="star-cell-right">
            <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
        </div>
        <div class="star-cell-left">
            @php
                $n = 0;
            @endphp
            @foreach($skills as $skill)
                @if($skill->SkillLevel == 1)
                    @php $n++; @endphp
                    <div class="skill-label trdlvl">
                        <a href="/viewskill/{{$skill->SkillID}}">{{$skill->SkillName}}</a>
                    </div>
                @endif
            @endforeach
            @if($n == 0)
                <h4>Компетенций 1го уровня нет</h4>
            @endif
        </div>
    </div>

    <div class="row v_line_1stlvl"></div>
    <div class="star-table">
        <div class="star-cell-right">
            <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
            <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
        </div>
        <div class="star-cell-left">
            @php
                $n = 0;
            @endphp
            @foreach($skills as $skill)
                @if($skill->SkillLevel == 2)
                    @php $n++; @endphp
                    <div class="skill-label sndlvl">
                        <a href="/viewskill/{{$skill->SkillID}}">{{$skill->SkillName}}</a>
                    </div>
                @endif
            @endforeach
            @if($n == 0)
                <h4>Компетенций 2го уровня нет</h4>
            @endif
        </div>
    </div>
    <div class="row v_line_1stlvl"></div>
    <div class="star-table">
        <div class="star-cell-right">
            <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
            <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
            <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
        </div>
        <div class="star-cell-left">
            @php
                $n = 0;
            @endphp
            @foreach($skills as $skill)
                @if($skill->SkillLevel == 3)
                    @php $n++; @endphp
                    <div class="skill-label trdlvl">
                        <a href="/viewskill/{{$skill->SkillID}}">{{$skill->SkillName}}</a>
                    </div>
                @endif
            @endforeach
            @if($n == 0)
                <h4>Компетенций 3го уровня нет</h4>
            @endif
        </div>
    </div>
    <div class="row v_line_1stlvl"></div>

    <script type="text/javascript">
        var userAD = {!! isset($user->ADName) ? json_encode(explode('\\', $user->ADName)) : 'undefined'; !!};
        console.log(userAD);
    </script>

@stop

@section('scripts')

    <script type="text/javascript">
        $(document).ready(function() {
            appendAvatar('#profile__avatar');
        });
    </script>

@stop
