@extends('layouts.main')

@section('content')

    <section class="content-header">
        <h1>
            Управление уровнями компетенций {{$FIO or ''}}
        </h1>
    </section>

    <main class="content">
        <div class="row">
            <div class="col-md-8">
                <form class="form" action="/riseuserskills" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_id" value="{{$user_id}}">
                    <table class="table">
                        <tr><th>Компетенция</th><th>Уровень</th></tr>
                        @foreach($skills as $skill)
                            <tr>
                                <td>{{$skill->SkillName}}</td>
                                <td>
                                    <label class="radio-inline"><input type="radio" value="0" name="Skills[{{$skill->SkillID}}]" @if($skill->SkillLevel == 0) checked="checked" @endif>0</label>
                                    <label class="radio-inline"><input type="radio" value="1" name="Skills[{{$skill->SkillID}}]" @if($skill->SkillLevel == 1) checked="checked" @endif>1</label>
                                    <label class="radio-inline"><input type="radio" value="2" name="Skills[{{$skill->SkillID}}]" @if($skill->SkillLevel == 2) checked="checked" @endif>2</label>
                                    <label class="radio-inline"><input type="radio" value="3" name="Skills[{{$skill->SkillID}}]" @if($skill->SkillLevel == 3) checked="checked" @endif>3</label>
                                </td>
                                <td>
                                    <a href="/removeuserskill/{{$user_id}}/{{$skill->SkillID}}"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <button type="submit" class="btn btn-success">Сохранить</button>
                </form>

                <hr>

                @if (isset($catalogStr))
                    <form class="form" action="/adduserskills" method="POST">
                        {!! csrf_field() !!}
                        <input type="hidden" name="user_id" value="{{$user_id}}">
                        {!! $catalogStr !!}
                        <button type="submit" class="btn btn-success pull-right">Добавить</button>
                    </form>
                @endif

            </div>
        </div>
    </main>

@stop

@section('scripts')

    <script type="text/javascript" src="{{ mix('js/rise.js') }}"></script>

@stop