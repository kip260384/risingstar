<aside class="main-sidebar">
  <!-- Inner sidebar -->
  <div class="sidebar">
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <!-- Optionally, you can add icons to the links -->
      <li {{{ (Request::is('/') ? 'class=active' : '') }}}>
        <a href="/">
          <i class="glyphicon icon-rs icon-rs-main"></i><span>Главная</span>
        </a>
      </li>
      <li {{{ (Request::is('about') ? 'class=active' : '') }}}>
        <a href="/about">
          <i class="glyphicon icon-rs icon-rs-system"></i><span>О системе</span>
        </a>
      </li>
      <li {{{ (Request::is('skills') ? 'class=active' : '') }}}>
        <a href="/skills">
          <i class="glyphicon icon-rs icon-rs-star"></i><span>Компетенции</span>
        </a>
      </li>
      <li {{{ (Request::is('engineers') ? 'class=active' : '') }}}>
        <a href="/engineers">
          <i class="glyphicon icon-rs icon-rs-people"></i><span>Сотрудники</span>
        </a>
      </li>

      @if (LdapAuth::hasRole('admin'))
        <li class="header">Меню администратора</li>
        <li {{{ (Request::is('add_news') ? 'class=active' : '') }}}>
          <a href="/add_news">
            <i class="glyphicon glyphicon-paste"></i><span>Добавить новость</span>
          </a>
        </li>
        <li {{{ (Request::is('skillcatalog') ? 'class=active' : '') }}}>
          <a href="/skillcatalog">
            <i class="glyphicon glyphicon-list"></i><span>Структура компетенций</span>
          </a>
        </li>
        <li {{{ (Request::is('admin/visits') ? 'class=active' : '') }}}>
          <a href="/admin/visits">
            <i class="glyphicon glyphicon-stats"></i><span>Статистика посещений</span>
          </a>
        </li>
        <li {{{ (Request::is('adm_positions') ? 'class=active' : '') }}}>
          <a href="/adm_positions">
            <i class="glyphicon glyphicon-barcode"></i><span>Профили должностей</span>
          </a>
        </li>
      @endif
      @if (LdapAuth::hasRole('manager'))
        <li class="header">Меню менеджера</li>
        <li {{{ (Request::is('manager_engineers') ? 'class=active' : '') }}}>
          <a href="/manager_engineers">
            <span>Управление уровнями</span>
          </a>
        </li>
        <li {{{ (Request::is('skillsdraft') ? 'class=active' : '') }}}>
          <a href="/skillsdraft">
            <span>Черновики компетенций</span>
          </a>
        </li>
        <li {{{ (Request::is('skillstorework') ? 'class=active' : '') }}}>
          <a href="/skillstorework">
            <span>У меня на доработке</span>
          </a>
        </li>
      @endif
      @if (LdapAuth::authorize('open_moderate'))
        <li class="header">Меню модератора</li>
        <li {{{ (Request::is('moderator') ? 'class=active' : '') }}}>
          <a href="/moderator">
            <i class="glyphicon glyphicon-star-empty"></i><span>Новые компетенции</span>
          </a>
        </li>
      @endif
      <!-- <li class="treeview">
        <a href="#"><span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li><a href="#">Link in level 2</a></li>
          <li><a href="#">Link in level 2</a></li>
        </ul>
      </li> -->
    </ul><!-- /.sidebar-menu -->
  </div><!-- /.sidebar -->
  <div class="sidebar__footer">
    <a href="mailto:Rising_Star@alfabank.ru" class="sidebar__footer__maillink"><span class="sidebar__footer__maillink__icon glyphicon glyphicon-envelope"></span><span class="sidebar__footer__maillink__text">rising_star@alfabank.ru</span></a>
  </div>
</aside><!-- /.main-sidebar -->