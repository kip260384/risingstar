@extends('layouts.main')

@section('content')

    <section class="content-header">
        <h1>
            Каталог компетенций
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> --> 
    </section>

    <main class="content">
        <div class="row">
            <div class="col-xs-8">
                @if (isset($catalogStr))
                    <form class="form" action="/del_category" method="POST">
                        {!! $catalogStr !!}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger pull-right">Удалить</button>
                    </form>
                @endif
            </div>
            <div class="col-xs-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form" action="/add_category" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="parent">Родитель</label>
                                <select class="form-control" id="parent" name="parent">
                                    @foreach ($catalog_flat as $row)
                                        <option value="{{$row['Value']}}">{{$row['DisplayText']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="name">Название категории</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Название" required>
                            </div>

                            <button type="submit" class="btn btn-success">Добавить</button>
                        </form>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form" action="/rename_category" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="id">Категория</label>
                                <select class="form-control" name="id">
                                    @foreach ($catalog_flat as $row)
                                        <option value="{{$row['Value']}}">{{$row['DisplayText']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="newname">Новое имя</label>
                                <input class="form-control" type="text" id="newname" name="newname" required>
                            </div>

                            <button type="submit" class="btn btn-success">Переименовать</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </main>
@stop

@section("scripts")
    <script type="text/javascript" src="{{ mix('js/skillcatalog.js') }}"></script>
@stop
