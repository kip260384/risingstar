@extends('layouts.main')

@section('content')

  <section class="content-header">
      <div class="row">
        <div class="col-xs-12">
          <form class="form-inline">
            <div class="form-group">
              <h3>Компетенции</h3>
            </div>
            <div class="form-group pull-right">
              <a class="btn btn-success navbar-btn" href="/addskill" role="button">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                Новая компетенция
              </a>
            </div>
          </form>
        </div>

      </div>

  </section>

  <main class="content">

    <div class="row">

      <div class="col-md-4">
        @include('skills.filter')
      </div>

      <div class="col-md-8">
        @include('skills.skills')
      </div>

    </div>


  </main>

@stop

@section('scripts')

  <script type="text/javascript" src="{{ mix('js/skills.js') }}"></script>

@stop