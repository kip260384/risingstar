@extends('layouts.main')

@section('content')

    <section class="content-header">
        <div class="row">
            <div class="col-xs-12">
                <form class="form-inline">
                    <div class="form-group">
                        <h3>{{$title or ""}}</h3>
                    </div>
                </form>
            </div>

        </div>

    </section>

    <main class="content">

        <div class="row">

            <div class="col-md-12">
                @include('widget.skills_table')
            </div>

        </div>


    </main>

@stop

@section('scripts')

    {{--<script type="text/javascript" src="{{ mix('js/skills.js') }}"></script>--}}

@stop