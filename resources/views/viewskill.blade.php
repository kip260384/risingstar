@extends('layouts.main')

@section('content')

    <section class="content-header">
        <br>
        <h1 class="lya_big_text">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Компетенция - {{$skill->SkillName}}
        </h1>
        <div class="underline"></div>

        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>

    <main class="content">
        @if(count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-xs-9">
                <div class="skill-desc">
                    {!! $skill->SkillDesc !!}
                </div>
                <br>
                <div class="row fst_desc text-center">
                    <div class="col-xs-1">
                        <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-11 text-left">
                        {!! $lvls[1]->Description !!}
                    </div>
                    <div class="">&nbsp;</div>
                    <div class="col-xs-12">
                        @php
                            $links = array();
                                if($lvls[1]->Test != ''){
                                    $dom = new DOMDocument();
                                    @$dom->loadHTML($lvls[1]->Test);

                                    $tags = $dom->getElementsByTagName('a');
                                    foreach ($tags as $tag) {
                                         $links[] =  $tag->getAttribute('href');
                                    }
                                }
                        @endphp
                        @foreach($links as $link)
                            <a href="{{$link}}" class="take_test" target="_blank">Пройти тест!</a>
                        @endforeach
                    </div>
                    <div class="skill_underline col-xs-6 col-xs-offset-3">
                        <br><br>
                    </div>
                </div>

                <div class="row fst_desc text-center">
                    <div class="col-xs-1" style="white-space: nowrap;">
                        <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
                        <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-11 text-left">
                        {!! $lvls[2]->Description !!}
                    </div>
                    <div class="">&nbsp;</div>
                    <div class="col-xs-12">
                        @php
                            $links = array();
                            if($lvls[2]->Test != ''){
                                $dom = new DOMDocument();
                                @$dom->loadHTML($lvls[2]->Test);

                                $tags = $dom->getElementsByTagName('a');
                                foreach ($tags as $tag) {
                                     $links[] =  $tag->getAttribute('href');
                                }
                            }
                        @endphp
                        @foreach($links as $link)
                            <a href="{{$link}}" class="take_test" target="_blank">Пройти тест!</a>
                        @endforeach
                    </div>
                    <div class="skill_underline col-xs-6 col-xs-offset-3">
                        <br><br>
                    </div>
                </div>

                <div class="row fst_desc text-center">
                    <div class="col-xs-1" style="white-space: nowrap;">
                        <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
                        <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
                        <span class="glyphicon glyphicon-star yellow" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-11 text-left">
                        {!! $lvls[3]->Description !!}
                    </div>
                    <div class="">&nbsp;</div>
                    <div class="col-xs-12">
                        @php
                            $links = array();
                            if($lvls[3]->Test != ''){
                                $dom = new DOMDocument();
                                @$dom->loadHTML($lvls[3]->Test);

                                $tags = $dom->getElementsByTagName('a');
                                foreach ($tags as $tag) {
                                     $links[] =  $tag->getAttribute('href');
                                }
                            }
                        @endphp
                        @foreach($links as $link)
                            <a href="{{$link}}" class="take_test" target="_blank">Пройти тест!</a>
                        @endforeach
                    </div>
                    <div class="skill_underline col-xs-6 col-xs-offset-3">
                        <br><br>
                    </div>
                </div>


            </div>
            <div class="col-xs-3 text-center lya_left_border">
                <p class="lya_big_text fs13em">Твой текущий уровень</p>
                <div class="lya_yourlevel">
                    {{$SkillLevel or '0'}}
                </div>
                <br>

                @foreach($men as $man)
                    @if($man->SMRolesID == 2)
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="text-center man_info_left lya_blue">
                                    Эксперт
                                    <div id="expert__avatar"></div>
                                    <!-- <img src="#" width="100%" height="100%"> -->
                                </div>
                                <div class="man_info_right">
                                    <div class="v_center lya_blue">
                                        {{$man->FIO}}
                                    </div>
                                </div>
                                <div class="col-xs-12 ask_man">
                                    <a href="#">Задать вопрос эксперту</a>
                                </div>
                            </div>
                        </div>
                        <br>
                    @endif
                @endforeach

                @foreach($men as $man)
                    @if($man->SMRolesID == 1)
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="text-center man_info_left lya_blue">
                                    Менеджер
                                    <div id="manager__avatar"></div>
                                    <!-- <img src="#" width="100%" height="100%"> -->
                                </div>
                                <div class="man_info_right">
                                    <div class="v_center lya_blue">
                                        {{$man->FIO}}
                                    </div>
                                </div>
                                <div class="col-xs-12 ask_man">
                                    <a href="#">Задать вопрос менеджеру</a>
                                </div>

                                <a href="#" class="want_study" data-toggle="modal" data-target="#m_materials">
                                    <div class="col-xs-12 ask_man">Хочу обучиться!</div>
                                </a>

                            </div>
                        </div>
                    @endif
                @endforeach
                <br>
                <div class="col-xs-12 ask_lvlup">
                    <a href="#" data-toggle="modal" data-target="#myModal">Сообщить о повышении уровня</a>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-xs-12"><br></div>
                    @if($admin or $manager)
                        <a href='/editskill/{{$skill->ID}}' class="btn btn-info"><span class='glyphicon glyphicon-pencil'></span> Редактировать</a>
                    @endif
                    @if(LdapAuth::authorize('move_skill_to_arhiv', $skill->ID) && $skill->Status != 3)
                        <a href='/movetoarhiv/{{$skill->ID}}' class="btn btn-info">В архив</a>
                    @endif
                </div>
            </div>
        </div>


        <hr>
        @if(LdapAuth::authorize('open_moderate') && $skill->Status == 1)
            <div class="panel panel-default">
                <div class="panel-heading">Модерация</div>
                <div class="panel-body">
                    <form class="form" action="/skill_rework" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{$skill->ID}}" name="skill_id">
                        <div class="form-group">
                            <label for="comment">Комментарий доработки</label>
                            <textarea id="comment" name="comment" class="form-control" rows="3"></textarea>
                        </div>
                        <div class="btn-group" role="group">
                            <a type="button" href="/activate_skill/{{$skill->ID}}" class="btn btn-default">Активировать</a>
                            <button type="submit" class="btn btn-default">На доработку</button>
                            <a href="/moderator" type="button" class="btn btn-default">Новые компетенции</a>
                        </div>
                    </form>
                </div>
            </div>
            <br>
        @endif

        @if($skill->Status == 5 or $skill->Status == 1)
            <div class="panel panel-default">
                <div class="panel-heading">Комментарий доработки</div>
                <div class="panel-body">
                    @foreach($comments as $comment)
                        {{$comment->Date}} - {{$comment->FIO}}<br>
                        {{$comment->Description}}
                        <br>
                    @endforeach
                </div>
            </div>
        @endif
    </main>

@stop

@section('scripts')

    <script type="text/javascript">
        var men = {!! json_encode($men); !!};
        if (men !== undefined) {
            $.each(men, function(i, element) {
                if (element.SMRolesID == 1) {
                    appendAvatar('#manager__avatar', element.ADName);
                }
                if (element.SMRolesID == 2) {
                    appendAvatar('#expert__avatar', element.ADName);
                }
            })
        }
    </script>

@stop

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Восходящая звезда</h4>
            </div>
            <div class="modal-body">
                Менеджеру направлен запрос на повышение уровня.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>

    </div>
</div>

<div id="m_materials" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Материалы для изучения</h4>
            </div>
            <div class="modal-body">
                {!! $skill->Materials or "Раздел не заполнен" !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>

    </div>
</div>