@if($Skills == false)
    <p>
        Компетенций не найдено
    </p>
@else()
    <table class="table">
        <tr>
            <th>Категория/Функия</th><th>Компетенция</th><th>Менеджер</th><th>Эксперт</th><th></th>
        </tr>
        @foreach($Skills as $skill)
            <tr>
                <td>{{$skill['fullpath']}}</td>
                <td>
                    <a href="viewskill/{{$skill['id']}}">{{$skill['name']}}</a>
                </td>
                <td>{{$skill['manager_fio']}}</td>
                <td>{{$skill['expert_fio']}}</td>
                <td></td>
            </tr>
        @endforeach
    </table>
@endif