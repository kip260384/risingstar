<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function() {
	return view('welcome');
});


Route::get('/', 'MainController@index');
Route::get('/test', 'MainController@test');
Route::get('/skills', 'MainController@skills');
Route::get('/profile/{id?}', 'MainController@profile');
Route::get('/engineers', 'MainController@engineers');
Route::get('/adm_positionprofile/{pos_id}', 'MainController@adm_positionprofile');
Route::get('/adm_positions', 'MainController@adm_positions');
Route::get('/addskill', 'SkillController@addskill');
Route::get('/delskill/{id}', 'SkillController@delskill');
Route::get('/editskill/{id}', 'SkillController@editskill');
Route::get('/viewskill/{id}', 'SkillController@viewskill');
//Route::get('/get_skills_by_cat', 'SkillController@get_skills_by_cat');
Route::post('/addskill_post', 'SkillController@addskill_post');
Route::get('/skillcatalog', 'CatalogController@skillcatalog');
Route::post('/add_category', 'CatalogController@add_category');
Route::post('/rename_category', 'CatalogController@rename_category');
Route::post('/del_category', 'CatalogController@del_category');
Route::get('/rise/{user_id}', 'MainController@rise');
Route::post('/adduserskills', 'SkillController@adduserskills');
Route::post('/add_position_skills', 'SkillController@add_position_skills');
Route::post('/riseuserskills', 'SkillController@riseuserskills');
Route::post('/save_position_skills', 'SkillController@save_position_skills');
Route::get('/removeuserskill/{user_id}/{skill_id}', 'SkillController@removeuserskill');
Route::post('/skill_rework', 'SkillController@skill_rework');
//Route::post('/skill_toapprove', 'SkillController@skill_toapprove');
Route::get('/skillstorework', 'SkillController@skillstorework');
Route::get('/skillsdraft', 'SkillController@skillsdraft');
Route::get('/activate_skill/{id}', 'SkillController@activate_skill');
Route::get('/movetoarhiv/{skill_id}', 'SkillController@movetoarhiv');
Route::get('/remove_position_skill/{pos_id}/{skill_id}', 'SkillController@remove_position_skill');

Route::get('/moderator', 'MainController@moderator');
Route::get('/manager_engineers', 'MainController@manager_engineers');

Route::get('/about', function() {
    return view('about');
});

Route::get('/news', 'NewsController@index');
Route::get('/add_news', 'NewsController@add_news');
Route::get('/del_news/{id}', 'NewsController@del_news');
Route::post('/add_news_post', 'NewsController@add_news_post');

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
    Route::get('/visits', 'AdminController@visits');
});

Route::group(['prefix' => 'api/v1'], function() {
    Route::get('engineers/filter', 'ApiController@engineers');
    Route::get('/getusersbyfio', 'ApiController@getusersbyfio');
    Route::get('/getallskills', 'ApiController@generateSkillsForBubbleChart');
    Route::get('/visitchart', 'ApiController@dataVisitChart');
    Route::get('/skills/radar/{EmployeeID?}', 'ApiController@dataRadarChart');
    Route::get('/get_skills_by_cat', 'ApiController@get_skills_by_cat');
    Route::get('/add_wishedcity', 'ApiController@add_wishedcity');
    Route::get('/del_wishedcity', 'ApiController@del_wishedcity');
    Route::get('/set_WishedPosition', 'ApiController@set_WishedPosition');
});

Route::group(['prefix' => 'profile'], function() {
    Route::get('{user_id}/cabinet', 'MainController@profile');
    Route::get('{user_id}/circle', 'MainController@circle');
    Route::get('{user_id}/rate', 'MainController@rate');
});

Route::get('/ldap', 'LdapController@test');

Route::get('/insertid', 'UtilsController@insertEmployeeID');

Route::get('/serverinfo', function() {
    echo(phpversion());
    echo("<br>");
    if(PHP_INT_SIZE == 4){
        echo("x32<br>");
    }
    if(PHP_INT_SIZE == 8){
        echo("x64<br>");
    }

    echo('_SERVER:<br>');
	dd($_SERVER);
});

Route::get('/confluence', function(){
    return redirect()->away('http://confluence/pages/viewpage.action?pageId=99125417');
});

Route::get('/demo', function(){
    return redirect()->away('http://s24-uts.regions.alfaintra.net:8088');
});