const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/dashboard.js', 'public/js')
    .js('resources/assets/js/mymorris.js', 'public/js')
    .js('resources/assets/js/skills.js', 'public/js')
    .js('resources/assets/js/rise.js', 'public/js')
    .js('resources/assets/js/engineers.js', 'public/js')
    .js('resources/assets/js/addskills.js', 'public/js')
    .js('resources/assets/js/skillcatalog.js', 'public/js')
    .js('resources/assets/js/rosechart.js', 'public/js')
    .js('resources/assets/js/radarchart.js', 'public/js')
    .js('resources/assets/js/myprofile.js', 'public/js')
    .js('resources/assets/js/adm_positionprofile.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .version()
    .sourceMaps()
    .browserSync({
        files: [
            'public/build/**/*.css',                     // This is the one required to get the CSS to inject
            'public/build/**/*.scss',                     // This is the one required to get the CSS to inject
            'resources/views/**/*.blade.php',       // Watch the views for changes & force a reload
            'app/**/*.php',                      // Watch the app files for changes & force a reload
            'modules/**/*.php'                      // Watch the app modules files for changes & force a reload
        ],
        proxy: 'http://starmap/',
        logPrefix: "Laravel Eixir BrowserSync",
        logConnections: false,
        reloadOnRestart: false,
        notify: false,
        open: false
    });

// mix.browserSync('http://localhost:8000/');
